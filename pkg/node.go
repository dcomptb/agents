package cogs

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"strings"
	"time"

	etcd "github.com/coreos/etcd/clientv3"

	log "github.com/sirupsen/logrus"
	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/tech/foundry/api"
	"gitlab.com/mergetb/tech/nex/pkg"
	"gitlab.com/mergetb/tech/sled/pkg"
	"gitlab.com/mergetb/xir/lang/go"
)

func NodeSetup(mzid, instance string, fragments []*site.MzFragment) error {

	return NodeOps(NodeSetupKind, mzid, instance, fragments)

}

func NodeRecycle(mzid, instance string, fragments []*site.MzFragment) error {

	return NodeOps(NodeRecycleKind, mzid, instance, fragments)

}

func NodeOps(kind, mzid, instance string, fragments []*site.MzFragment) error {

	fields := log.Fields{"mzid": mzid, "instance": instance}

	topo, err := LoadXIR()
	if err != nil {
		return common.ErrorEF("failed to load testbed xir", err, fields)
	}

	mzinfo, err := mzInfo(mzid, instance)
	if err != nil {
		return common.ErrorEF("failed to fetch mzinfo", err, fields)
	}

	svcaddr := SvcCidr(mzinfo.Vindex)
	addr, _, err := net.ParseCIDR(svcaddr)
	if err != nil {
		return common.ErrorEF("failed to parse svcaddr", err, fields)
	}

	withFoundry := true
	if kind == NodeRecycleKind {
		withFoundry = false
	}

	var actions []*Action

	for _, fragment := range fragments {
		for _, resource := range fragment.Resources {

			affiliateNode(mzid, resource)
			spec, err := nodeSpec(
				topo, instance, resource, fragment, addr, mzinfo, withFoundry)
			if err != nil {
				return err
			}
			actions = append(actions, &Action{
				Kind:       kind,
				Mzid:       mzid,
				MzInstance: instance,
				Action:     fmt.Sprintf("%s (%s)", spec.Resource, spec.Xname),
				Data:       spec,
			})

		}
	}

	err = Write(mzinfo)
	if err != nil {
		return common.ErrorE("failed to save mzinfo", err)
	}

	err = LaunchTask(
		NewTask(
			NewStage(actions...),
		),
	)
	if err != nil {
		return common.ErrorE("failed to launch node setup task", err)
	}

	return nil

}

func forEachNode(
	fragments []*site.MzFragment,
	f func(id string, resource *xir.Node, props xir.Props) error,
) error {

	topo, err := LoadXIR()
	if err != nil {
		return err
	}

	for _, fragment := range fragments {
		for _, resource := range fragment.Resources {

			fields := log.Fields{"resource": resource}

			// get the node from the testbed topology
			_, _, node := topo.GetNode(resource)
			if node == nil {
				log.WithFields(fields).Warn("request for unknown node")
				return fmt.Errorf("node %s does not exist", resource)
			}

			model := fragment.GetNodeModel()
			var data xir.Props
			if model != nil {
				data := xir.Props(props(model.Model))
				if data == nil {
					log.WithFields(fields).Warn("bad or missing node data")
					log.Warnf("%#v", fragment.Model)
				}
			}

			err = f(resource, node, data)
			if err != nil {
				return err
			}

		}
	}

	return nil

}

func forEachLink(
	fragments []*site.MzFragment,
	f func(id string, resource *xir.Link, props xir.Props) error,
) error {

	topo, err := LoadXIR()
	if err != nil {
		return err
	}

	for _, fragment := range fragments {
		for _, resource := range fragment.Resources {

			fields := log.Fields{"resource": resource}

			// get the node from the testbed topology
			_, _, link := topo.GetLink(resource)
			if link == nil {
				log.WithFields(fields).Warn("request for unknown link")
				return fmt.Errorf("link %s does not exist", resource)
			}

			model := fragment.GetLinkModel()
			data := props(model.Model)
			if data == nil {
				log.WithFields(fields).Warn("bad or missing node data")
				log.Warnf("%#v", fragment.Model)
			}

			err = f(resource, link, xir.Props(data))
			if err != nil {
				return err
			}

		}
	}

	return nil

}

//NodeInfraInfo something struct
type NodeInfraInfo struct {
	Endpoint Endpoint
	Image    string
	Mac      string
	Name     string
}

func nodeInfraInfo(id string, resource *xir.Node, props xir.Props) (*NodeInfraInfo, error) {

	fields := log.Fields{"resource": resource}

	// endpoint
	ie := GetInfraEndpoint(resource)
	if ie == nil {
		log.WithFields(fields).Error("node has no infra connections")
		return nil, fmt.Errorf("internal model error")
	}

	ifname, ok := ie.Props["name"].(string)
	if !ok {
		log.WithFields(fields).Error("node infra endpoint has no name")
		return nil, fmt.Errorf("internal model error")
	}

	// mac
	mac, ok := ie.Props["mac"].(string)
	if !ok {
		log.WithFields(fields).Error("node infra endpoint has no mac")
		return nil, fmt.Errorf("internal model error")
	}

	// image
	image := defaultSledImage
	imageConstraint := ExtractConstraint(props, "image")
	if imageConstraint != nil {
		imageValue, ok := imageConstraint.Value.(*sled.Image)
		if ok {
			image = imageValue
		}
	}

	return &NodeInfraInfo{
		Endpoint: Endpoint{
			Node:      resource.Id,
			Interface: ifname,
		},
		Mac:   mac,
		Image: image.Name,
	}, nil

}

func fragmentError(f *site.MzFragment, msg string) error {

	f.State = site.MzFragment_Error
	f.Error = msg

	log.WithFields(log.Fields{
		"fragment":  f.Id,
		"resources": f.Resources,
	}).Error(f.Error)

	return fmt.Errorf(f.Error)

}

func ListNodes() ([]*NodeStatus, error) {

	var result []*NodeStatus
	err := withEtcd(func(c *etcd.Client) error {

		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		resp, err := c.Get(ctx, "/nodestatus", etcd.WithPrefix())
		cancel()
		if err != nil {
			return err
		}

		for _, kv := range resp.Kvs {

			ns := &NodeStatus{}
			err := json.Unmarshal(kv.Value, ns)
			if err != nil {
				return fmt.Errorf("failed to unmarshal node status: %v", err)
			}
			result = append(result, ns)

		}

		return nil

	})
	if err != nil {
		return nil, err
	}

	return result, nil

}

func nodeSpec(
	topo *xir.Net, instance, name string, fragment *site.MzFragment, svc net.IP,
	mzinfo *MzInfo, withFoundry bool,
) (*NodeSpec, error) {

	fields := log.Fields{"mzid": mzinfo.Mzid, "node": name}

	_, _, node := topo.GetNode(name)
	if node == nil {
		return nil, common.ErrorF("node not found", fields)
	}

	model := fragment.GetNodeModel()
	if model == nil {
		return nil, common.ErrorF("node model not found", fields)
	}
	data := props(model.Model)
	if data == nil {
		log.WithFields(fields).Warn("bad or missing node data")
		log.Warnf("%#v", fragment.Model)
	}

	ie := GetInfraEndpoint(node)
	if ie == nil {
		return nil, common.ErrorF("missing infra network endpoint", fields)
	}

	// get the mac
	mac, ok := ie.Props["mac"].(string)
	if !ok {
		return nil, common.ErrorF("missing mac", fields)
	}
	mac = strings.ToLower(mac)

	// get the interface name
	ifname, ok := ie.Props["name"].(string)
	if !ok {
		return nil, common.ErrorF("missing interface name", fields)
	}

	// get the experiment specified node name otherwise use the node id
	xname, ok := data["name"].(string)
	if !ok {
		xname = fragment.Elements[0]
	}

	binding, err := userImageData(data)
	if err != nil {
		return nil, common.ErrorF("bad image data", fields)
	}

	// get kernel append info for the selected node
	if binding.Image.Append == "" {
		kappend, ok := node.Props["append"].(string)
		if !ok {
			return nil, common.ErrorF("missing kernel append line", fields)
		}
		binding.Image.Append = kappend

		rootdev, ok := node.Props["rootdev"].(string)
		if !ok {
			return nil, common.ErrorF("missing root device", fields)
		}
		binding.Image.Blockdevice = rootdev
	}

	var mc *foundry.MachineConfig
	if withFoundry {
		mc, err = machineConfig(mzinfo.Mzid, model, node)
		if mc != nil {
			mc.Id = mac
		}
		if err != nil {
			log.WithFields(fields).Warn("failed to create foundry config")
		}
	}

	ns := &NodeSpec{
		Resource:    name,
		Xname:       xname,
		Mac:         mac,
		Interface:   ifname,
		Binding:     binding,
		Config:      mc,
		SvcEndpoint: svc.String(),
		Infranet: &TargetNetwork{
			Vni: mzinfo.Vni,
			Vid: mzinfo.Vid,
		},
	}

	mzinfo.NodeInfo[name] = NodeInfo{
		XpName: xname,
		Image:  binding,
		Config: mc,
		Nex: &nex.Member{
			Mac:  mac,
			Name: name,
		},
	}

	return ns, nil

}

func affiliateNode(mzid, node string) {

	status := &NodeStatus{Name: node}
	err := ReadNew(status)
	if err != nil {
		common.ErrorEF("failed to read node status", err, log.Fields{
			"node": node,
			"mzid": mzid,
		})
		return
	}
	status.Mzid = mzid

	err = Write(status)
	if err != nil {
		common.ErrorEF("failed to write node status", err, log.Fields{
			"node": node,
			"mzid": mzid,
		})
	}

}
