package cogs

import (
	"net"

	log "github.com/sirupsen/logrus"

	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/mergetb/tech/rtnl"
)

type EnclaveSpec struct {
	Mzid            string
	Vindex          int
	ExternalIfx     string
	ExternalGateway string
	ExternalSubnet  string
	InternalGateway string
	NatSourceIP     string
	Interfaces      []*EnclaveVeth

	ver int64
}

type MacAdvSpec struct {
	Netns     string
	Interface string
	Vni       int
	Vindex    int
}

func CreateIfx(netns string, ifx *EnclaveVeth) error {

	defCtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return common.ErrorE("rtnl default context", err)
	}
	defer defCtx.Close()

	tgtCtx, err := rtnl.OpenContext(netns)
	if err != nil {
		return common.ErrorE("rtnl context", err)
	}
	defer tgtCtx.Close()

	err = ReadNew(ifx)
	if err != nil {
		return common.ErrorE("ifx read", err)
	}

	ip, nw, err := net.ParseCIDR(ifx.Address)
	if err != nil {
		return common.ErrorE("parse cidr", err)
	}

	veth, _, err := ensureVethPair(ifx.Inner, ifx.Outer, defCtx, tgtCtx, ip,
		nw.Mask)
	if err != nil {
		return common.ErrorE("ensure veth pair", err)
	}
	ifx.CtrMac = veth.Info.Address.String()
	err = Write(ifx)
	if err != nil {
		return common.ErrorE("veth writeback", err)
	}

	err = enslaveVeth(ifx.Outer, ifx.Bridge, ifx.Vid)
	if err != nil {
		return common.ErrorE("enslave veth", err)
	}

	if ifx.EvpnAdvertise {

		err = EvpnAdvertiseMulticast(ifx.Vni, ifx.Vindex)
		if err != nil {
			return common.ErrorE("advertise mcast", err)
		}

		err = EvpnAdvertiseMac(veth, ifx.Vni, ifx.Vindex)
		if err != nil {
			return common.ErrorE("advertise mac", err)
		}

	}

	return nil

}

func DeleteIfx(netns string, ifx *EnclaveVeth) error {

	defCtx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return common.ErrorE("rtnl default context", err)
	}
	defer defCtx.Close()

	veth, err := rtnl.GetLink(defCtx, ifx.Outer)
	if err == nil {

		if ifx.EvpnAdvertise {

			err = EvpnWithdrawMulticast(ifx.Vni, ifx.Vindex)
			if err != nil {
				return common.ErrorE("advertise mcast", err)
			}

			err = EvpnWithdrawMac(veth, ifx.Vni, ifx.Vindex)
			if err != nil {
				return common.ErrorE("advertise mac", err)
			}

		}

		err := veth.Absent(defCtx)
		if err != nil {
			log.WithError(err).WithFields(log.Fields{
				"name": ifx.Outer,
			}).Warn("failed to delete veth")
		}

	}

	return nil

}

func ensureVethPair(
	ctrIfname, hostIfname string,
	defaultCtx, targetCtx *rtnl.Context,
	ip net.IP, prefix net.IPMask,
) (*rtnl.Link, *rtnl.Link, error) {

	log.Printf("ensure veth pair")

	// create eth<X> for the container
	ctrIf := &rtnl.Link{
		Info: &rtnl.LinkInfo{
			Name: ctrIfname,
			Ns:   uint32(targetCtx.Fd()),
			Veth: &rtnl.Veth{
				Peer: hostIfname,
			},
		},
	}
	err := ctrIf.Add(targetCtx)
	if err != nil {

		// nothing to do
		if err.Error() == "file exists" {
			log.Warnf("%s exists already - skipping veth setup", ctrIfname)

			// collect the other side of the veth peer and send back
			hostIf, err := rtnl.GetLink(defaultCtx, hostIfname)
			if err != nil {
				return ctrIf, nil, common.ErrorE("failed to get resident veth peer", err)
			}
			return ctrIf, hostIf, nil
		}

		// badness
		return nil, nil, common.ErrorE("failed to create container eth0", err)

	}

	err = ctrIf.SetMtu(targetCtx, common.GetConfig().Net.Mtu)
	if err != nil {
		return nil, nil, common.ErrorE("failed to set ctr-veth mtu", err)
	}

	err = ctrIf.Up(targetCtx)
	if err != nil {
		return nil, nil, common.ErrorE("failed to bring up container eth0", err)
	}

	// set container interface's ip address
	addr := &rtnl.Address{
		Info: &rtnl.AddrInfo{
			Address: &net.IPNet{
				IP:   ip,
				Mask: prefix,
			},
		},
	}

	err = ctrIf.AddAddr(targetCtx, addr)
	if err != nil {
		return nil, nil, common.ErrorE("failed to assign eth0 address", err)
	}

	// put host side of the veth pair in the default namespace
	hostIf, err := rtnl.GetLink(targetCtx, hostIfname)
	if err != nil {
		return nil, nil, common.ErrorE("failed to get host interface", err)
	}

	err = hostIf.SetMtu(targetCtx, common.GetConfig().Net.Mtu)
	if err != nil {
		return nil, nil, common.ErrorE("failed to set host-veth mtu", err)
	}

	hostIf.Info.Ns = uint32(defaultCtx.Fd())
	err = hostIf.Set(targetCtx)
	if err != nil {
		return nil, nil, common.ErrorE("failed to place host interface default netns", err)
	}

	return ctrIf, hostIf, nil

}

func enslaveVeth(vethName, bridgeName string, vid int) error {

	ctx := &rtnl.Context{}

	fields := log.Fields{
		"veth":   vethName,
		"bridge": bridgeName,
	}

	// get a handle to the veth device
	veth, err := rtnl.GetLink(ctx, vethName)
	if err != nil {
		return common.ErrorEF("failed to get veth device", err, fields)
	}

	// get a handle to the bridge
	bridge, err := rtnl.GetLink(ctx, bridgeName)
	if err != nil {
		return common.ErrorEF("failed to get bridge device", err, fields)
	}

	// if the veth device is not already on the bridge, enslave it
	if veth.Info.Master != uint32(bridge.Msg.Index) {

		veth.Info.Master = uint32(bridge.Msg.Index)

		err = veth.Set(ctx)
		if err != nil {
			return common.ErrorEF("failed to add veth to bridge", err, fields)
		}

		if vid != 0 {

			err = veth.SetUntagged(ctx, uint16(vid), false, true, false)
			if err != nil {
				return common.ErrorEF("failed to set veth bridge vlan tag", err, fields)
			}

			err = veth.SetUntagged(ctx, 1, true, true, false)
			if err != nil {
				return common.ErrorEF("failed to remove default vlan from veth", err, fields)
			}

			err = bridge.SetUntagged(ctx, uint16(vid), false, false, true)
			if err != nil {
				return common.ErrorEF("failed to set bridge vlan tag", err, fields)
			}

		}
	}

	// ensure the veth device is up
	err = veth.Up(ctx)
	if err != nil {
		return common.ErrorEF("failed to bring up veth", err, fields)
	}

	return nil

}

func CreateInfrapodNS(name string) error {

	_, err := ensureMznNetworkNamespace(name)
	if err != nil {
		return common.ErrorE("ensure ns", err)
	}
	return nil

}

func DeleteInfrapodNS(name string) error {

	return destroyMznNetworkNamespace(name)

}
