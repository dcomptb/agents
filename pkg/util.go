package cogs

import (
	"fmt"
)

func short(s string) string {

	return s[:8]

}

// sigh, needed for data that gets cross-serialized, nfc what the number format
// will be, json it's int64, mapstructure+yaml its int
func GetIntLike(x interface{}) (int, error) {

	i, ok := x.(int)
	if !ok {
		i_, ok := x.(float64)
		if !ok {
			return -1, fmt.Errorf("not int like'%#v %T'", x, x)
		}
		i = int(i_)
	}
	return i, nil

}
