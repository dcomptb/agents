package cogs

import (
	"encoding/json"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"

	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/tech/foundry/api"
	"gitlab.com/mergetb/xir/lang/go"
	"gitlab.com/mergetb/xir/lang/go/tb"
)

const (
	FoundryNamespace       = "foundry"
	FoundryImage           = "docker.io/mergetb/foundry:latest"
	FoundryCertMountName   = "foundry-cert"
	FoundryCertMountSource = "/etc/foundry/manage.pem"
	FoundryCertMountDest   = "/etc/foundry/manage.pem"
	FoundryKeyMountName    = "foundry-key"
	FoundryKeyMountSource  = "/etc/foundry/manage-key.pem"
	FoundryKeyMountDest    = "/etc/foundry/manage-key.pem"
	FoundryEnv             = "FOUNDRYD_ARGS"
	FoundryArgs            = "-no-auth"
)

// extract machine configuration infromation from merge experiment specs
func machineConfig(
	mzid string,
	model *site.NodeModel,
	tbnode *xir.Node,
) (*foundry.MachineConfig, error) {

	data := props(model.Model)

	// extract the base machine config
	mc, err := foundryConfig(data)
	if err != nil {
		return nil, err
	}

	// configure the node's interfaces
	err = ifconfig(mzid, mc, model, tbnode)
	if err != nil {
		return nil, err
	}

	// expand rootfs by default
	mc.Config.ExpandRootfs = true

	return mc, nil

}

func ifconfig(mzid string, cfg *foundry.MachineConfig, model *site.NodeModel,
	tbnode *xir.Node) error {

	ifmap := make(map[string][]*site.InterfaceModel)
	for _, ifx := range model.Interfaces {

		ifmap[ifx.Phyid] = append(ifmap[ifx.Phyid], ifx)

	}

	for phy, ifxs := range ifmap {
		for _, ifx := range ifxs {

			ep := tbnode.GetEndpointId(phy)
			if ep == nil {
				return common.ErrorF("endpoint not found", log.Fields{
					"id": phy,
				})
			}

			fi := &foundry.Interface{Name: ep.Label()}

			// pickup any base configuration from the interface model
			model := props(ifx.Model)
			bc := basicIfxConfig(model)
			if bc != nil {
				fi.Addrs = bc.Ip.Addrs
				fi.Mtu = uint32(bc.Ip.Mtu)
			}

			// if there is more than one xp-interface per physical interface, need to
			// break them out over vlans
			if len(ifxs) > 1 {

				// fetch the testbed link info for the vlid
				li := &LinkInfo{Mzid: mzid, Vlid: ifx.Vlid}
				err := ReadWait(li, nil)
				if err != nil {
					return common.ErrorEF("failed to read linkinfo", err, log.Fields{
						"vlid": ifx.Vlid,
					})
				}

				fi.Vlan = &foundry.Vlan{Vid: int32(li.Vid)}

			}

			cfg.Config.Interfaces = append(cfg.Config.Interfaces, fi)

		}
	}

	return nil

}

func foundryConfig(data map[string]interface{}) (*foundry.MachineConfig, error) {

	info, ok := data["foundry"]
	if !ok {
		return nil, common.Error("missing foundry data")
	}

	cfg := &foundry.MachineConfig{}
	err := mapstructure.Decode(info, cfg)
	if err != nil {
		return nil, common.ErrorE("failed to parse foundry config", err)
	}

	return cfg, nil

}

func basicIfxConfig(data map[string]interface{}) *tb.BasicInterfaceConfig {

	cfg := &tb.BasicInterfaceConfig{}
	err := mapstructure.Decode(data, cfg)
	if err != nil {
		log.WithError(err).Warn("no basic interface config found")
		return nil
	}

	return cfg

}

func props(data string) map[string]interface{} {

	m := make(map[string]interface{})
	err := json.Unmarshal([]byte(data), &m)
	if err != nil {
		log.WithError(err).Warn("failed to unmarhsal props")
		return nil
	}

	return m

}
