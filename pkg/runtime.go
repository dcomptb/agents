package cogs

import (
	"encoding/binary"
	"fmt"
	"net"

	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/mergetb/tech/foundry/api"
	"gitlab.com/mergetb/tech/nex/pkg"
	"gitlab.com/mergetb/tech/sled/pkg"
)

type NodeStatus struct {
	Name string
	Mzid string
	Sled SledStatus

	ver int64
}

type SledStatus struct {
	State string
	Time  string
}

type NodeInfo struct {
	XpName string
	Image  *ImageBinding
	Config *foundry.MachineConfig
	Nex    *nex.Member
}

type MzInfo struct {
	Mzid     string
	Instance string
	Vindex   int
	Vni      int
	Vid      int

	IncomingTaskId string
	TeardownTaskId string

	NodeSetupTaskId   string
	NodeRecycleTaskId string

	LinkCreateTaskId  string
	LinkDestroyTaskId string

	NodeInfo map[string]NodeInfo
	LinkInfo []LinkSpec

	ver int64
}

type LinkInfo struct {
	Mzid string
	Vlid string // virtual link identifier from merge

	Vni int
	Vid int

	ver int64
}

const (
	InfrapodMznIfx             = "eth0"
	InfrapodSvcIfx             = "eth1"
	svcbr                      = "svcbr"
	defaultInfraSubnet         = "172.30.0.0/16"
	defaultInfraGateway        = "172.30.0.1/16"
	defaultInfraSubnetBegin    = "172.30.0.1"
	defaultInfraDhcpBegin      = "172.30.0.10"
	defaultInfraSubnetEnd      = "172.30.255.255"
	defaultSvcStart            = "172.31.0.10"
	defaultSvcPrefix           = 16
	vindexPoolSize             = 1 << 24
	vniPoolSize                = 1 << 24
	vidPoolSize                = 4096
	DefaultInternalGateway     = "172.30.0.1"
	DefaultInternalGatewayCIDR = "172.30.0.1/16"
	DefaultServiceGateway      = "172.31.0.1"

	HarborVni = 2
	HarborVid = 2

	vindexKey   = "vindex"
	infraVniKey = "ivni"
	infraVidKey = "ivid"
	xpVniKey    = "xvni"
	xpVidKey    = "xvid"

	HarborControlAddr = "172.31.0.2"
)

var defaultSledImage = &sled.Image{
	Name:      "debian:buster",
	Kernel:    "debian-buster-kernel",
	Initramfs: "debian-buster-initramfs",
	Disk:      "debian-buster-disk",
	Append:    "console=ttyS0 console=ttyS1 root=/dev/sda1 rootfstype=ext4 rw earlyprintk=ttyS1",
}

func harborMzInfo() *MzInfo {
	return &MzInfo{
		Mzid:   "harbor.dcomptb.io",
		Vindex: 2,
		Vni:    2,
		Vid:    2,
	}
}

func mzInfo(mzid, instance string) (*MzInfo, error) {

	mzinfo := &MzInfo{
		Mzid:     mzid,
		NodeInfo: make(map[string]NodeInfo),
	}
	err := ReadNew(mzinfo)
	if err != nil {
		return nil, common.ErrorE("mzinfo read failure", err)
	}
	mzinfo.Instance = instance
	// if the info already exists just return it
	if mzinfo.ver > 0 {
		return mzinfo, nil
	}

	vindex := CountSet{Name: vindexKey, Size: vindexPoolSize, Offset: 100}
	vni := CountSet{Name: infraVniKey, Size: vniPoolSize, Offset: 100}
	vid := CountSet{Name: infraVidKey, Size: vidPoolSize, Offset: 10}

	err, _ = ReadObjects([]Object{&vindex, &vni, &vid})
	if err != nil {
		return nil, common.ErrorE("vset read failed", err)
	}
	vindex.Offset = 100
	vni.Offset = 100
	vid.Offset = 10

	vindexNext, vindex, err := vindex.Add()
	if err != nil {
		return nil, common.ErrorE("vindex next failed", err)
	}

	vniNext, vni, err := vni.Add()
	if err != nil {
		return nil, common.ErrorE("vni next failed", err)
	}

	vidNext, vid, err := vid.Add()
	if err != nil {
		return nil, common.ErrorE("vid next failed", err)
	}

	mzinfo.Vindex = vindexNext
	mzinfo.Vni = vniNext
	mzinfo.Vid = vidNext

	err = WriteObjects([]Object{&vindex, &vni, &vid, mzinfo})
	if err != nil {
		return nil, common.ErrorE("vset writeback failed", err)
	}

	return mzinfo, nil

}

func delLinkInfo(mzid, vlid string) (*LinkInfo, error) {

	li := &LinkInfo{
		Mzid: mzid,
		Vlid: vlid,
	}
	err := ReadNew(li)
	if err != nil {
		return nil, common.ErrorE("linkinfo read failure", err)
	}
	// if the info already exists just return it
	if li.ver == 0 {
		return li, nil
	}

	vni := CountSet{Name: xpVniKey, Size: vniPoolSize, Offset: 100}
	vid := CountSet{Name: xpVidKey, Size: vidPoolSize, Offset: 10}

	err, _ = ReadObjects([]Object{&vni, &vid})
	if err != nil {
		return nil, common.ErrorE("vni/vid read failed", err)
	}

	vni = vni.Remove(li.Vni)
	vid = vid.Remove(li.Vid)

	// update everything in one shot txn
	otx := ObjectTx{
		Put:    []Object{&vni, &vid},
		Delete: []Object{li},
	}
	err = RunObjectTx(otx)
	if err != nil {
		return nil, common.ErrorE("vni/vid/linkinfo writeback failed", err)
	}

	return li, nil

}

func initLinkInfo(mzid, vlid string) (*LinkInfo, error) {

	li := &LinkInfo{
		Mzid: mzid,
		Vlid: vlid,
	}
	err := ReadNew(li)
	if err != nil {
		return nil, common.ErrorE("linkinfo read failure", err)
	}
	// if the info already exists just return it
	if li.ver > 0 {
		return li, nil
	}

	vni := CountSet{Name: xpVniKey, Size: vniPoolSize, Offset: 100}
	vid := CountSet{Name: xpVidKey, Size: vidPoolSize, Offset: 10}

	err, _ = ReadObjects([]Object{&vni, &vid})
	if err != nil {
		return nil, common.ErrorE("vni/vid read failed", err)
	}
	vni.Offset = 100
	vid.Offset = 10

	vniNext, vni, err := vni.Add()
	if err != nil {
		return nil, common.ErrorE("vni next failed", err)
	}

	vidNext, vid, err := vid.Add()
	if err != nil {
		return nil, common.ErrorE("vid next failed", err)
	}

	li.Vni = vniNext
	li.Vid = vidNext

	err = WriteObjects([]Object{&vni, &vid, li})
	if err != nil {
		return nil, common.ErrorE("vni/vid writeback failed", err)
	}

	return li, nil

}

func SvcCidr(vindex int) string {

	return fmt.Sprintf("%s/%d", svcAddr(vindex).String(), defaultSvcPrefix)

}

func SvcAddress(vindex int) string {

	return fmt.Sprintf("%s", svcAddr(vindex).String())

}

func svcAddr(vindex int) net.IP {

	begin := binary.BigEndian.Uint32([]byte(net.ParseIP(defaultSvcStart).To4()))
	next := begin + uint32(vindex)

	buf := make([]byte, 4)
	binary.BigEndian.PutUint32(buf, next)
	return net.IP(buf)

}

func mzbr(index int) string   { return fmt.Sprintf("mzbr%d", index) }
func ifr(index int) string    { return fmt.Sprintf("ifr%d", index) }
func svceth(index int) string { return fmt.Sprintf("svc%d", index) }
