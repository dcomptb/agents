package cogs

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"reflect"
	"strings"
	"sync"
	"time"

	etcd "github.com/coreos/etcd/clientv3"
	"github.com/teris-io/shortid"

	//https://github.com/go-yaml/yaml/issues/139
	"github.com/mergetb/yaml/v3"

	log "github.com/sirupsen/logrus"

	. "gitlab.com/dcomptb/cogs/common"
)

const (
	TimeFormat = "2 Jan 06 15:04:05.00 MST"
	IdAlphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_@"
)

var ()

type Task struct {
	Id        string `yaml:",omitempty"`
	Coglet    string `yaml:",omitempty"`
	Stages    []*Stage
	Deps      []string `yaml:",omitempty"`
	Timestamp string

	mtx     sync.Mutex
	version int64
}

func NewTask(stages ...*Stage) *Task {

	return &Task{Stages: stages}

}

func (t *Task) Init() {

	for i := 0; i < len(t.Stages)-1; i++ {
		t.Stages[i].Next = t.Stages[i+1]
	}

}

func (t *Task) AddDep(id string) {

	for _, x := range t.Deps {
		if x == id {
			return
		}
	}

	t.Deps = append(t.Deps, id)

}

func (t *Task) Mzid() string {
	if t == nil {
		return ""
	}
	if len(t.Stages) == 0 {
		return ""
	}
	return t.Stages[0].Mzid()
}

func (t *Task) AddStage(s *Stage) *Stage {

	t.mtx.Lock()
	defer t.mtx.Unlock()
	t.Stages[len(t.Stages)-1].Next = s
	t.Stages = append(t.Stages, s)
	return s

}

func (t *Task) InsertStage(after *Stage, new *Stage) *Stage {

	t.mtx.Lock()
	defer t.mtx.Unlock()

	for i, s := range t.Stages {
		if s == after {

			new.Next = after.Next
			after.Next = new

			if i == len(t.Stages)-1 {
				t.Stages = append(t.Stages, new)
				return new
			}

			t.Stages = append(t.Stages, nil)
			copy(t.Stages[i+2:], t.Stages[i+1:])
			t.Stages[i+1] = new
			return new

		}
	}

	log.Warn("insert target not found")
	t.Stages = append(t.Stages, new)
	return new

}

func (t *Task) Key() string        { return fmt.Sprintf("/task/%s", t.Id) }
func (t *Task) GetVersion() int64  { return t.version }
func (t *Task) SetVersion(v int64) { t.version = v }
func (t *Task) Value() interface{} { return t }
func (t *Task) Complete() bool {
	for _, s := range t.Stages {
		for _, a := range s.Actions {
			if a == nil {
				continue
			}
			if !a.Complete && !a.Masked {
				return false
			}
		}
	}
	return true
}

func (t *Task) Failed() []*Action {
	var result []*Action = nil
	for _, s := range t.Stages {
		for _, a := range s.Actions {
			if a == nil {
				continue
			}
			if a.Error != nil && !a.Masked {
				result = append(result, a)
			}
		}
	}
	return result
}

func (t *Task) ClearErrors() {
	for _, s := range t.Stages {
		for _, a := range s.Actions {
			if a == nil {
				continue
			}
			a.Error = nil
		}
	}
}

func (t *Task) Reset() {
	for _, s := range t.Stages {
		for _, a := range s.Actions {
			if a == nil {
				continue
			}
			a.Error = nil
			a.Complete = false
		}
	}
}

func (t *Task) MaskAction(stage, action int) {

	t.Stages[stage].Actions[action].Masked = true

}

func (t *Task) DepsSatisfied() bool {

	for _, id := range t.Deps {

		dep := &Task{Id: id}

		err := Read(dep)
		if err != nil {
			log.WithFields(log.Fields{"task": t.Id, "dep": id}).Warn(
				"failed to read task dependency")
			continue
		}

		if !dep.Complete() {
			return false
		}

	}

	return true

}

func (t *Task) Wait() error {

	err := Read(t)
	if err != nil {
		return err
	}

	for !t.Complete() && t.Failed() == nil {
		time.Sleep(250 * time.Millisecond)
		Read(t)
	}

	return nil

}

type Stage struct {
	Actions []*Action
	Next    *Stage `json:"-" yaml:"-" mapstructure:"-"`
}

func (s *Stage) Mzid() string {
	if s == nil {
		return ""
	}
	if len(s.Actions) == 0 {
		return ""
	}
	return s.Actions[0].Mzid
}

func NewStage(actions ...*Action) *Stage {

	return &Stage{Actions: actions}

}

type MzOperation string

const (
	MzOutgoing MzOperation = "outgoing"
	MzIncoming MzOperation = "incoming"
	MzMod      MzOperation = "mod"
)

type Action struct {
	Kind       string
	Mzid       string
	MzOp       MzOperation
	MzInstance string
	Action     string
	Data       interface{}
	Complete   bool
	Error      *string
	Masked     bool

	task  *Task
	stage *Stage
}

func (a *Action) Task() *Task     { return a.task }
func (a *Action) SetTask(t *Task) { a.task = t }

func (a *Action) Stage() *Stage     { return a.stage }
func (a *Action) SetStage(s *Stage) { a.stage = s }

// List all tasks
func ListTasks() ([]*Task, error) {

	var tasks []*Task
	err := withEtcd(func(c *etcd.Client) error {

		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		resp, err := c.Get(ctx, "/task", etcd.WithPrefix())
		cancel()
		if err != nil {
			return err
		}

		for _, kv := range resp.Kvs {

			id, err := extractTaskId(string(kv.Key))
			if err != nil {
				return err
			}
			t := &Task{Id: id}
			err = Read(t)
			if err != nil {
				log.
					WithError(err).
					WithFields(log.Fields{"id": id}).
					Error("unable to read task")
				continue
			}

			tasks = append(tasks, t)

		}

		return nil

	})
	if err != nil {
		return nil, err
	}

	return tasks, nil

}

// Launch the specified task
func LaunchTask(task *Task) error {

	if task.Id == "" {

		sid, err := shortid.New(1, IdAlphabet, 4747)
		if err != nil {
			log.Fatal(err)
		}
		id := sid.MustGenerate()
		task.Id = id
		task.Timestamp = time.Now().Format(TimeFormat)

	}

	// check for existing tasks with this mzid
	err := addMzidDeps(task)
	if err != nil {
		return err
	}

	return Write(task)

}

// Cancel tasks with the specified prefix
func CancelTask(prefix string) error {

	return withEtcd(func(cli *etcd.Client) error {

		key := fmt.Sprintf("/task/%s", prefix)
		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		_, err := cli.Delete(ctx, key, etcd.WithPrefix())
		cancel()

		return err

	})

}

func extractTaskId(key string) (string, error) {

	parts := strings.Split(key, "/")
	if len(parts) != 3 {
		return "", fmt.Errorf("malformed task key '%s'", key)
	}

	return parts[2], nil

}

func HandleTask(
	etcd *etcd.Client,
	task *Task,
	lease etcd.LeaseID,
	doTask func(*Task) error,
) {

	fields := log.Fields{
		"id": short(task.Id),
	}

	log.WithFields(fields).Trace("handling task")

	// keep the lease alive while the task is executing
	done, err := KeepLeaseAlive(etcd, lease)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("keepalive failed")
		return
	}

	// actually execute the task
	err = doTask(task)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("task failed")
	}

	// kill the keepalive thread
	done <- true

	// write updated task back to db, even a partial failure may have made
	// progress
	err = Write(task)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("task writeback failed")
	}

	// either way we revoke the lease, if we failed, another lower half will
	// come along and try to drive this to completion
	RevokeLease(etcd, lease)
	log.WithFields(fields).Info("task complete")

}

func LeaseTask(c *etcd.Client, t *Task) (etcd.LeaseID, error) {

	//get lease grant
	lease, err := c.Grant(context.TODO(), int64(GetConfig().Tuning.TaskLease))
	if err != nil {
		log.WithError(err).Error("failed to get lease grant")
		return 0, fmt.Errorf("lease failed")
	}

	// try to lease the task
	kvc := etcd.NewKV(c)
	leasekey := fmt.Sprintf("/lease/%s", t.Id)
	txr, err := kvc.Txn(context.TODO()).
		If(
			etcd.Compare(etcd.Version(leasekey), "=", 0),
		).
		Then(
			etcd.OpPut(leasekey, clid, etcd.WithLease(lease.ID)),
		).
		Commit()
	if err != nil {
		log.WithError(err).Error("transaction error")
		return 0, fmt.Errorf("transaction error")
	}
	if !txr.Succeeded {
		//This means that the criteriea was not met, which likely just means that
		//another coglet has task or all state is gtg (target == current)
		log.WithFields(log.Fields{"lease": leasekey}).Trace("taken")
		return 0, nil
	}

	return lease.ID, nil

}

func ReadTask(file string) (*Task, error) {

	buf, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	*yaml.DefaultMapType = reflect.TypeOf(map[string]interface{}{})

	t := &Task{}
	err = yaml.Unmarshal(buf, t)
	if err != nil {
		return nil, err
	}

	return t, nil

}

func addMzidDeps(task *Task) error {

	return withEtcd(func(c *etcd.Client) error {

		ctx, cancel := context.WithTimeout(context.Background(), 1*time.Second)
		tasks, err := c.Get(ctx, "/task/", etcd.WithPrefix())
		cancel()
		if err != nil {
			return ErrorE("error fetching cog tasks", err)
		}
		for _, x := range tasks.Kvs {

			t := &Task{}
			err := json.Unmarshal(x.Value, t)
			if err != nil {
				return err
			}

			if t.Mzid() == task.Mzid() && (!t.Complete() || t.Failed() != nil) {
				task.AddDep(t.Id)
			}

		}

		return nil

	})

}
