package cogs

import (
	"context"
	"fmt"
	"time"

	"github.com/coreos/etcd/clientv3"
	log "github.com/sirupsen/logrus"
	. "gitlab.com/dcomptb/cogs/common"
)

//Try up to 10 times to connect to etcd
func EtcdConnect() (*clientv3.Client, error) {
	log.Trace("connecting to etcd...")
	etcd, err := EtcdClient()
	if err == nil {
		return etcd, nil
	}
	for i := 0; i < 10; i++ {
		ErrorE("error connecting to etcd", err)
		time.Sleep(1 * time.Second)
		etcd, err = EtcdClient()
		if err == nil {
			return etcd, nil
		}
	}
	return nil, ErrorE("failed to connect to etcd", err)
}

func EnsureEtcd(etcdp **clientv3.Client) error {

	c := GetConfig()
	connstr := fmt.Sprintf("%s:%d", c.Etcd.Address, c.Etcd.Port)

	// ensure we have a usable etcd connection
	var err error
	if *etcdp == nil {
		log.Trace("etcd connection nil - connecting")
		*etcdp, err = EtcdConnect()
		if err != nil {
			return err
		}
	}

	// check etcd status, if foobar reconnect
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	_, err = (*etcdp).Status(ctx, connstr)
	cancel()
	if err != nil {

		log.WithFields(log.Fields{
			"err": err,
		}).Warning("etcd status check error - reconnecting")

		(*etcdp).Close()
		*etcdp, err = EtcdConnect()
		if err != nil {
			return err
		}

		ctx, cancel := context.WithTimeout(context.Background(), timeout)
		_, err = (*etcdp).Status(ctx, connstr)
		cancel()
		if err != nil {
			return ErrorE("etcd status check failed - giving up", err)
		}
	}

	return err

}

func withEtcd(f func(*clientv3.Client) error) error {

	cli, err := EtcdConnect()
	if err != nil {
		return err
	}
	defer cli.Close()

	return f(cli)

}
