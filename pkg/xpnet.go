package cogs

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/xir/lang/go"
	"gitlab.com/mergetb/xir/lang/go/tb"
)

func CreateLinks(mzid, instance string, fragments []*site.MzFragment) error {

	return modifyLinks(mzid, instance, fragments, SetCanopyLinks)

}

func DestroyLinks(mzid, instance string, fragments []*site.MzFragment) error {

	return modifyLinks(mzid, instance, fragments, RemoveCanopyLinks)

}

func modifyLinks(mzid, instance string, fragments []*site.MzFragment, action string) error {

	fields := log.Fields{
		"op":        "modifyLinks",
		"mzid":      mzid,
		"fragments": len(fragments),
	}
	log.WithFields(fields).Info("modifying link fragments")

	topo, err := LoadXIR()
	if err != nil {
		return common.ErrorEF("xir load failed", err, fields)
	}

	// stage to collect all canopy create-link actions in
	stage := &Stage{}

	var linkinfo []LinkSpec

	for _, fragment := range fragments {

		// punt on multi-element links
		if len(fragment.Elements) != 1 {
			fields["fragment"] = *fragment
			return common.ErrorF("multi-element links not supported", fields)
		}
		//xpid := fragment.Elements[0]

		lnk := fragment.GetLinkModel()
		if lnk == nil {
			log.Warnf("%#v", fragment.Model)
			return common.Error("could not read fragment link model")
		}
		model := props(lnk.Model)

		// if there are properties associated with the link, that means it needs
		// emulation so setup a moa link
		if isMoaLink(model) {
			return modifyMoaLink(mzid, fragment, action)
		}

		// initialize/delete the data for this link
		var li *LinkInfo
		switch action {
		case SetCanopyLinks:

			li, err = initLinkInfo(mzid, lnk.Vlid)
			if err != nil {
				return common.ErrorEF("link info init failed", err, fields)
			}

		case RemoveCanopyLinks:

			li, err = delLinkInfo(mzid, lnk.Vlid)
			if err != nil {
				return common.ErrorEF("link info delete failed", err, fields)
			}

		default:
			fields["case"] = action
			common.ErrorF("undefined remove link action", fields)
			continue

		}

		linkspec := LinkSpec{
			Vni:   li.Vni,
			Vid:   li.Vid,
			Links: fragment.Resources,
			Kind:  XpLink,
		}

		for _, resource := range fragment.Resources {

			fields["resource"] = resource
			log.WithFields(fields).Info("handling link resource")

			// fetch the link from the testbed model
			_, _, tblink := topo.GetLink(resource)
			if tblink == nil {
				fields["link"] = resource
				return common.ErrorF("link does not exist", fields)
			}

			// if this is an edge link, we need to create a canopy spec
			var nodeE *xir.Endpoint
			switch {
			case tb.HasRole(tblink.Endpoints[0].Parent, tb.Node):

				nodeE = tblink.Endpoints[0]
				//switchE = tblink.Endpoints[1]

			case tb.HasRole(tblink.Endpoints[1].Parent, tb.Node):

				nodeE = tblink.Endpoints[1]
				//switchE = tblink.Endpoints[0]

			default:
				continue // not an edge link
			}

			// get the current edgeport state
			multidegree := false
			for link, width := range lnk.Lanes {
				if resource == link && width > 1 {
					multidegree = true
				}
			}

			linkspec.Endpoints = append(linkspec.Endpoints, Endpoint{
				Node:        nodeE.Parent.Label(),
				Interface:   nodeE.Label(),
				Multidegree: multidegree,
			})

		}

		// create the canopy action with the link spec just created
		stage.Actions = append(stage.Actions, &Action{
			Kind:       CanopyKind,
			Mzid:       mzid,
			MzInstance: instance,
			Action:     action,
			Data:       linkspec,
		})

		log.WithFields(log.Fields{
			"linkspec": linkspec,
		}).Info("linkspec")

		linkinfo = append(linkinfo, linkspec)

	}

	mzinfo, err := mzInfo(mzid, instance)
	if err != nil {
		return common.ErrorEF("mzid init failed", err, fields)
	}
	mzinfo.LinkInfo = linkinfo
	err = Write(mzinfo)
	if err != nil {
		return common.ErrorEF("failed to write back mzinfo", err, fields)
	}

	t := &Task{Stages: []*Stage{stage}}
	err = LaunchTask(t)
	if err != nil {
		return common.ErrorEF(
			"failed to launch experiment linkup stage", err, fields)
	}

	return nil

}

//TODO(erik)
func modifyMoaLink(mzid string, fragment *site.MzFragment, action string) error {

	fields := log.Fields{
		"mzid":     mzid,
		"fragment": fmt.Sprintf("%+v", *fragment),
	}

	log.WithFields(fields).Info("handling moa link")

	return nil

}

type VLMentry struct {
	Vni int
	Vid int
}

type VLMap struct {
	Mzid    string
	Entries map[int]*VLMentry

	ver int64
}

func isMoaLink(props map[string]interface{}) bool {

	return false

}
