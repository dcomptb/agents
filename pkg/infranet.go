package cogs

import (
	"fmt"
	"net"

	"github.com/opencontainers/runtime-spec/specs-go"
	log "github.com/sirupsen/logrus"
	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/mergetb/tech/nex/pkg"
)

const (
	EtcdNamespace = "etcd"
	EtcdImage     = "docker.io/mergetb/etcd:latest"
)

// init ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func InitInfranet(mzid, instance string, props map[string]interface{}, record bool) error {

	mzinfo, err := mzInfo(mzid, instance)
	if err != nil {
		return err
	}

	// create actions

	createEnclave := enclaveAction(mzinfo)

	nexCtr, err := nexCtrAction(mzinfo)
	if err != nil {
		return err
	}

	foundryCtr, err := foundryCtrAction(mzinfo)
	if err != nil {
		return err
	}

	etcdCtr, err := etcdCtrAction(mzinfo)
	if err != nil {
		return err
	}

	nexInit, err := nexCreateNetworkActions(mzinfo)
	if err != nil {
		return err
	}

	nexStaticMembers, err := nexStaticMembersAction(mzinfo)
	if err != nil {
		return err
	}

	task := NewTask(
		NewStage(
			createEnclave,
		),
		NewStage(
			addVtep(mzinfo),
			nexCtr,
			foundryCtr,
			etcdCtr,
		),
		NewStage(
			nexInit...,
		),
		NewStage(
			nexStaticMembers,
		),
	)

	err = LaunchTask(task)
	if err != nil {
		return common.ErrorE("failed to launch init task", err)
	}

	if record {
		mzinfo.IncomingTaskId = task.Id
		err = Write(mzinfo)
		if err != nil {
			return common.ErrorE("failed to update mzinfo", err)
		}
	}

	return nil

}

// actions .......

func addVtep(mzinfo *MzInfo) *Action { return modVtep(mzinfo, false) }
func delVtep(mzinfo *MzInfo) *Action { return modVtep(mzinfo, true) }

func modVtep(mzinfo *MzInfo, remove bool) *Action {

	cfg := common.GetConfig()

	action := SetCanopyServiceVtep
	if remove {
		action = RemoveCanopyServiceVtep
	}

	return &Action{
		Kind:       CanopyKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     action,
		Data: &VtepSpec{
			Vni: mzinfo.Vni,
			Vid: mzinfo.Vid,
			Vteps: []Vtep{{
				Node:     InfraCanopyHost,
				Dev:      cfg.Net.VtepIfx,
				TunnelIP: cfg.Net.ServiceTunnelIP,
				Bridge:   mzbr(mzinfo.Vindex),
			}},
		},
	}

}

func harborLinks(mzinfo *MzInfo, endpoints []Endpoint, remove bool) *Action {

	action := SetCanopyLinks
	if remove {
		action = RemoveCanopyLinks
	}

	harborMzinfo := harborMzInfo()

	return &Action{
		Kind:       CanopyKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     action,
		Data: &LinkSpec{
			Kind:      InfraLink,
			Vni:       harborMzinfo.Vni,
			Vid:       harborMzinfo.Vid,
			Endpoints: endpoints,
		},
	}

}

func nexCtrAction(mzinfo *MzInfo) (*Action, error) {

	return &Action{
		Kind:       ContainerKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     LaunchCtr,
		Data: ContainerSpec{
			Container: Container{
				Name:      "nex",
				Namespace: mzinfo.Mzid,
				Vindex:    mzinfo.Vindex,
				Image:     NexImage,
			},
			Mounts: []*ContainerMount{{
				Name: NexMountName,
				Mount: specs.Mount{
					Source:      NexMountSource,
					Destination: NexMountDest,
					Options:     DefaultMountOptions,
					Type:        DefaultMountType,
				},
			}},
			Environment: map[string]string{
				NexDomainEnv: mzinfo.Mzid,
			},
		},
	}, nil

}

func foundryCtrAction(mzinfo *MzInfo) (*Action, error) {

	return &Action{
		Kind:       ContainerKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     LaunchCtr,
		Data: ContainerSpec{
			Container: Container{
				Name:      "foundry",
				Namespace: mzinfo.Mzid,
				Vindex:    mzinfo.Vindex,
				Image:     FoundryImage,
			},
			Mounts: []*ContainerMount{
				{
					Name: FoundryCertMountName,
					Mount: specs.Mount{
						Source:      FoundryCertMountSource,
						Destination: FoundryCertMountDest,
						Options:     DefaultMountOptions,
						Type:        DefaultMountType,
					},
				},
				{
					Name: FoundryKeyMountName,
					Mount: specs.Mount{
						Source:      FoundryKeyMountSource,
						Destination: FoundryKeyMountDest,
						Options:     DefaultMountOptions,
						Type:        DefaultMountType,
					},
				},
			},
			Environment: map[string]string{
				FoundryEnv: FoundryArgs,
			},
		},
	}, nil

}

func etcdCtrAction(mzinfo *MzInfo) (*Action, error) {

	return &Action{
		Kind:       ContainerKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     LaunchCtr,
		Data: ContainerSpec{
			Container: Container{
				Name:      "etcd",
				Namespace: mzinfo.Mzid,
				Vindex:    mzinfo.Vindex,
				Image:     EtcdImage,
			},
		},
	}, nil

}

func nexMembersAction(mzinfo *MzInfo, members []*nex.Member) (*Action, error) {

	svcaddr := SvcCidr(mzinfo.Vindex)
	addr, _, err := net.ParseCIDR(svcaddr)
	if err != nil {
		return nil, common.ErrorEF("failed to parse svcaddr", err, log.Fields{
			"mzid": mzinfo.Mzid,
		})
	}

	return &Action{
		Kind:       NexKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     AddNexMembers,
		Data: &NexMemberSpec{
			Endpoint: addr.String(),
			MemberList: nex.MemberList{
				Net:  mzinfo.Mzid,
				List: members,
			},
		},
	}, nil

}

func harborMembers(mzinfo *MzInfo, members []*nex.Member) *Action {

	harborMzinfo := harborMzInfo()

	return &Action{
		Kind:       NexKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     AddNexMembers,
		Data: &NexMemberSpec{
			Endpoint: HarborControlAddr,
			MemberList: nex.MemberList{
				Net:  harborMzinfo.Mzid,
				List: members,
			},
		},
	}

}

func nexCreateNetworkActions(mzinfo *MzInfo) ([]*Action, error) {

	svcaddr := SvcCidr(mzinfo.Vindex)
	addr, _, err := net.ParseCIDR(svcaddr)
	if err != nil {
		return nil, common.ErrorEF("failed to parse svcaddr", err, log.Fields{
			"mzid": mzinfo.Mzid,
		})
	}

	createActions := []*Action{
		// node network
		{
			Kind:       NexKind,
			Mzid:       mzinfo.Mzid,
			MzInstance: mzinfo.Instance,
			Action:     CreateNexNetwork,
			Data: &NexNetworkSpec{
				Endpoint: addr.String(),
				Network: nex.Network{
					Name:        mzinfo.Mzid,
					Subnet4:     defaultInfraSubnet,
					Gateways:    []string{DefaultInternalGateway},
					Nameservers: []string{defaultInfraSubnetBegin},
					Dhcp4Server: defaultInfraSubnetBegin,
					Domain:      mzinfo.Mzid,
					Range4: &nex.AddressRange{
						Begin: defaultInfraDhcpBegin,
						End:   defaultInfraSubnetEnd,
					},
				},
			},
		},
		// infrastructure network
		{
			Kind:       NexKind,
			Mzid:       mzinfo.Mzid,
			MzInstance: mzinfo.Instance,
			Action:     CreateNexNetwork,
			Data: &NexNetworkSpec{
				Endpoint: addr.String(),
				Network: nex.Network{
					Name:        fmt.Sprintf("static.%s", mzinfo.Mzid),
					Subnet4:     defaultInfraSubnet,
					Gateways:    []string{defaultInfraGateway},
					Nameservers: []string{defaultInfraSubnetBegin},
					Dhcp4Server: defaultInfraSubnetBegin,
					Domain:      mzinfo.Mzid,
				},
			},
		},
	}

	return createActions, nil

}

func nexStaticMembersAction(mzinfo *MzInfo) (*Action, error) {

	svcaddr := SvcCidr(mzinfo.Vindex)
	addr, _, err := net.ParseCIDR(svcaddr)
	if err != nil {
		return nil, common.ErrorEF("failed to parse svcaddr", err, log.Fields{
			"mzid": mzinfo.Mzid,
		})
	}

	// infrastructure members
	return &Action{
		Kind:       NexKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     AddNexMembers,
		Data: &NexMemberSpec{
			Endpoint: addr.String(),
			MemberList: nex.MemberList{
				Net: fmt.Sprintf("static.%s", mzinfo.Mzid),
				List: []*nex.Member{
					{
						Mac:  "00:00:00:00:00:01",
						Name: "foundry",
						Ip4:  &nex.Lease{Address: defaultInfraSubnetBegin},
					},
				},
			},
		},
	}, nil

}

func enclaveAction(mzinfo *MzInfo) *Action {

	cfg := common.GetConfig()
	svcaddr := SvcCidr(mzinfo.Vindex)

	return &Action{
		Kind:       PlumbingKind,
		Mzid:       mzinfo.Mzid,
		MzInstance: mzinfo.Instance,
		Action:     CreateEnclave,
		Data: &EnclaveSpec{
			Mzid:            mzinfo.Mzid,
			Vindex:          mzinfo.Vindex,
			ExternalIfx:     cfg.Net.ExternalIfx,
			ExternalGateway: cfg.Net.ExternalGateway,
			ExternalSubnet:  cfg.Net.ExternalSubnet,
			InternalGateway: DefaultInternalGateway,
			NatSourceIP:     cfg.Net.ExternalIP,
			Interfaces: []*EnclaveVeth{
				{
					Inner:         InfrapodMznIfx,
					Outer:         ifr(mzinfo.Vindex),
					Vni:           mzinfo.Vni,
					Vid:           mzinfo.Vid,
					EvpnAdvertise: true,
					Bridge:        mzbr(mzinfo.Vindex),
					Address:       defaultInfraGateway,
				},
				{
					Inner:   InfrapodSvcIfx,
					Outer:   svceth(mzinfo.Vindex),
					Bridge:  svcbr,
					Address: svcaddr,
				},
			},
		},
	}

}

// teardown ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func TeardownInfranet(mzid, instance string, props map[string]interface{}, record bool) error {

	mzinfo, err := mzInfo(mzid, instance)
	if err != nil {
		return err
	}

	t := NewTask(
		// delete interfaces
		NewStage(
			&Action{
				Kind:       PlumbingKind,
				Mzid:       mzid,
				MzInstance: instance,
				Action:     DestroyEnclave,
				Data:       &EnclaveSpec{
					// read in from datastore
				},
			},
			delVtep(mzinfo),
		),
		// delete tasks
		NewStage(
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzid,
				MzInstance: instance,
				Action:     DeleteCtrTask,
				Data: &DeleteContainerData{
					Name:      "nex",
					Namespace: mzid,
				},
			},
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzid,
				MzInstance: instance,
				Action:     DeleteCtrTask,
				Data: &DeleteContainerData{
					Name:      "etcd",
					Namespace: mzid,
				},
			},
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzid,
				MzInstance: instance,
				Action:     DeleteCtrTask,
				Data: &DeleteContainerData{
					Name:      "foundry",
					Namespace: mzid,
				},
			},
		),
		// delete containers
		NewStage(
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzid,
				MzInstance: instance,
				Action:     DeleteCtr,
				Data: &DeleteContainerData{
					Name:      "nex",
					Namespace: mzid,
				},
			},
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzid,
				MzInstance: instance,
				Action:     DeleteCtr,
				Data: &DeleteContainerData{
					Name:      "etcd",
					Namespace: mzid,
				},
			},
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzid,
				MzInstance: instance,
				Action:     DeleteCtr,
				Data: &DeleteContainerData{
					Name:      "foundry",
					Namespace: mzid,
				},
			},
		),
		NewStage(
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzid,
				MzInstance: instance,
				Action:     DeleteCtrRecord,
				Data: &DeleteContainerData{
					Name:      "nex",
					Namespace: mzid,
				},
			},
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzid,
				MzInstance: instance,
				Action:     DeleteCtrRecord,
				Data: &DeleteContainerData{
					Name:      "foundry",
					Namespace: mzid,
				},
			},
			&Action{
				Kind:       ContainerKind,
				Mzid:       mzid,
				MzInstance: instance,
				Action:     DeleteCtrRecord,
				Data: &DeleteContainerData{
					Name:      "etcd",
					Namespace: mzid,
				},
			},
		),
	)
	if record {
		t.Stages = append(t.Stages, NewStage(
			&Action{
				Kind:       BookkeepingKind,
				Mzid:       mzid,
				MzInstance: instance,
				Action:     DeleteMzinfo,
			},
		))
	}
	t.Init()

	err = LaunchTask(t)
	if err != nil {
		return common.ErrorE("failed to launch task", err)
	}

	vindex := CountSet{Name: vindexKey}
	vni := CountSet{Name: infraVniKey}
	vid := CountSet{Name: infraVidKey}
	vobj := []Object{&vindex, &vni, &vid}

	err, n := ReadObjects(vobj)
	if err != nil || n != 3 {
		common.Error("failed to read virtual network counters")
	}

	vindex = vindex.Remove(mzinfo.Vindex)
	vni = vni.Remove(mzinfo.Vni)
	vid = vid.Remove(mzinfo.Vid)

	vobj = []Object{&vindex, &vni, &vid}
	err = WriteObjects(vobj)
	if err != nil {
		common.Error("failed to write virtual network counters")
	}

	mzinfo.TeardownTaskId = t.Id
	err = Write(mzinfo)
	if err != nil {
		common.ErrorE("failed to update mzinfo", err)
	}

	return nil

}
