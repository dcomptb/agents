package cogs

import (
	"context"
	"fmt"
	"strings"
	"time"

	etcd "github.com/coreos/etcd/clientv3"
	"github.com/opencontainers/runtime-spec/specs-go"
	"gitlab.com/mergetb/site/api"
)

// leaf port state ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

type LeafPortState struct {
	Mzid string
	Leaf string
	Port string
	Mode LeafPortMode
	Vids []int

	version int64
}

type LeafPortMode int

const (
	Access LeafPortMode = iota
	Trunk
)

func (x *LeafPortState) Key() string {
	return fmt.Sprintf("/leaf/%s/%s", x.Leaf, x.Port)
}

func (x *LeafPortState) GetVersion() int64 {
	return x.version
}

func (x *LeafPortState) SetVersion(v int64) {
	x.version = v
}

func (x *LeafPortState) Value() interface{} {
	return x
}

// container ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

type Container struct {
	Name string
	//CtrNamespace string
	//NetNamespace string
	Namespace string
	Vindex    int
	Image     string

	version int64
}

func (x *Container) Key() string {
	return fmt.Sprintf("/ctr/%s/%s", x.Namespace, x.Name)
}
func (x *Container) GetVersion() int64  { return x.version }
func (x *Container) SetVersion(v int64) { x.version = v }
func (x *Container) Value() interface{} { return x }

type CtrComponent struct {
	CtrName      string `yaml:",omitempty"`
	CtrNamespace string `yaml:",omitempty"`
}

type EnclaveVeth struct {
	Inner         string
	Outer         string
	Vni           int  `yaml:",omitempty"`
	Vid           int  `yaml:",omitempty"`
	Vindex        int  `yaml:",omitempty"`
	EvpnAdvertise bool `yaml:",omitempty"`
	Bridge        string
	Address       string
	HostMac       string `yaml:",omitempty"`
	CtrMac        string `yaml:",omitempty"`

	version int64
}

func (x *EnclaveVeth) Key() string {
	return fmt.Sprintf("/eve/%d/%s", x.Vindex, x.Outer)
}
func (x *EnclaveVeth) GetVersion() int64  { return x.version }
func (x *EnclaveVeth) SetVersion(v int64) { x.version = v }
func (x *EnclaveVeth) Value() interface{} { return x }

type ContainerMount struct {
	Name        string
	specs.Mount `mapstructure:",squash" yaml:",inline"`

	CtrComponent `mapstructure:",squash" yaml:",inline"`

	version int64
}

func (x *ContainerMount) Key() string {
	return fmt.Sprintf("/ctr-mnt/%s/%s/%s", x.CtrNamespace, x.CtrName, x.Name)
}
func (x *ContainerMount) GetVersion() int64  { return x.version }
func (x *ContainerMount) SetVersion(v int64) { x.version = v }
func (x *ContainerMount) Value() interface{} { return x }

type ContainerSpec struct {
	Container `mapstructure:",squash" yaml:",inline"`
	//Interfaces  []*ContainerInterface
	Mounts      []*ContainerMount
	Environment map[string]string

	version int64
}

type NsnatSpec struct {
	Src     string
	Dst     string
	Addr    string
	Gateway string
}

// network namespace ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Network Namespace Record
type NetNS struct {
	Name       string
	Containers []string

	version int64
}

func (x *NetNS) Key() string        { return fmt.Sprintf("/netns/%s", x.Name) }
func (x *NetNS) GetVersion() int64  { return x.version }
func (x *NetNS) SetVersion(v int64) { x.version = v }
func (x *NetNS) Value() interface{} { return x }
func (x *NetNS) RemoveContainer(ctr string) {

	var ctrs []string

	for _, c := range x.Containers {
		if c != ctr {
			ctrs = append(ctrs, c)
		}
	}

	x.Containers = ctrs

}

// Vxlan Record
type VxLan struct {
	Vni        int
	Interfaces []string

	version int64
}

func (x *VxLan) Key() string        { return fmt.Sprintf("/vxlan/%d", x.Vni) }
func (x *VxLan) GetVersion() int64  { return x.version }
func (x *VxLan) SetVersion(v int64) { x.version = v }
func (x *VxLan) Value() interface{} { return x }
func (x *VxLan) RemoveInterface(mac string) {

	var ifxs []string

	for _, i := range x.Interfaces {
		if i != mac {
			ifxs = append(ifxs, i)
		}
	}

	x.Interfaces = ifxs

}

// countset ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (c *CountSet) Key() string        { return "/counters/" + c.Name }
func (c *CountSet) GetVersion() int64  { return c.ver }
func (c *CountSet) SetVersion(v int64) { c.ver = v }
func (c *CountSet) Value() interface{} { return c }

// mzinfo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (m *MzInfo) Key() string        { return "/mzinfo/" + m.Mzid }
func (m *MzInfo) GetVersion() int64  { return m.ver }
func (m *MzInfo) SetVersion(v int64) { m.ver = v }
func (m *MzInfo) Value() interface{} { return m }

// linkinfo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (l *LinkInfo) Key() string {
	return fmt.Sprintf("/linkinfo/%s/%s", l.Mzid, l.Vlid)
}
func (l *LinkInfo) GetVersion() int64  { return l.ver }
func (l *LinkInfo) SetVersion(v int64) { l.ver = v }
func (l *LinkInfo) Value() interface{} { return l }

// mz fragment ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

type MzfIndex struct {
	Resource string
	Fragment *site.MzFragment

	ver int64
}

func (x *MzfIndex) Key() string        { return "/mzf/" + x.Resource }
func (l *MzfIndex) GetVersion() int64  { return l.ver }
func (l *MzfIndex) SetVersion(v int64) { l.ver = v }
func (l *MzfIndex) Value() interface{} { return l }

// helper functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func ListMaterializations(prefix string) ([]string, error) {

	var result []string
	err := withEtcd(func(c *etcd.Client) error {

		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		resp, err := c.Get(ctx, "/mzinfo/"+prefix, etcd.WithPrefix())
		cancel()
		if err != nil {
			return err
		}

		for _, kv := range resp.Kvs {

			parts := strings.Split(string(kv.Key), "/")
			if len(parts) >= 3 {
				result = append(result, parts[2])
			}

		}

		return nil

	})
	if err != nil {
		return nil, err
	}

	return result, nil

}

// enclave ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (e *EnclaveSpec) Key() string {
	return fmt.Sprintf("/enclave/%s", e.Mzid)
}
func (e *EnclaveSpec) GetVersion() int64  { return e.ver }
func (e *EnclaveSpec) SetVersion(v int64) { e.ver = v }
func (e *EnclaveSpec) Value() interface{} { return e }

// vlmap ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (v *VLMap) Key() string {
	return fmt.Sprintf("/vlmap/%s", v.Mzid)
}
func (v *VLMap) GetVersion() int64  { return v.ver }
func (v *VLMap) SetVersion(x int64) { v.ver = x }
func (v *VLMap) Value() interface{} { return v }

// vlmap ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (n *NodeStatus) Key() string {
	return fmt.Sprintf("/nodestatus/%s", n.Name)
}
func (v *NodeStatus) GetVersion() int64  { return v.ver }
func (v *NodeStatus) SetVersion(x int64) { v.ver = x }
func (v *NodeStatus) Value() interface{} { return v }
