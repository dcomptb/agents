#!/bin/bash

if [[ $# -ne 2 ]]; then
  echo "usage: enclave <name> <index>"
  exit 1
fi

name=$1
i=$2

ip netns del $name
ip link del vrf$i
ip link del mzbr$i
ip link del ifr$i
ip link del svc$i
