#!/bin/bash

set -e

cat > ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "4700h"
    },
    "profiles": {
      "dcomptb": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "4700h"
      }
    }
  }
}
EOF

cat > ca-csr.json <<EOF
{
  "CN": "dcomptb",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Marina del Rey",
      "ST": "California",
      "O": "dcomptb",
      "OU": "CA"
    }
  ]
}
EOF

cat > cmdr-csr.json <<EOF
{
  "CN": "dcomptb:cmdr",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Marina del Rey",
      "ST": "California",
      "O": "dcomptb",
      "OU": "datastores"
    }
  ]
}
EOF

cat > manage-csr.json <<EOF
{
  "CN": "foundry:manage",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "NYC",
      "ST": "NY",
      "O": "foundry",
      "OU": "manage"
    }
  ]
}
EOF

cat > wgd-csr.json <<EOF
{
  "CN": "dcomptb:wgd",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "NYC",
      "ST": "NY",
      "O": "dcomptb",
      "OU": "wireguard"
    }
  ]
}
EOF

if [[ ! -f cfssl ]]; then
  curl -o cfssl -L https://pkg.cfssl.org/R1.2/cfssl_linux-amd64
  chmod +x cfssl
fi

if [[ ! -f cfssljson ]]; then
  curl -o cfssljson -L https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64
  chmod +x cfssljson
fi

# generate certificate authority
if [[ ! -f ca.pem ]]; then
  echo "generating ca"
  ./cfssl gencert -initca ca-csr.json | ./cfssljson -bare ca
fi

if [[ ! -f cmdr.pem ]]; then
echo "generating cmdr"
./cfssl gencert                                         \
  -ca=ca.pem                                            \
  -ca-key=ca-key.pem                                    \
  -config=ca-config.json                                \
  -hostname=spineleaf.mergetb.test,localhost,127.0.0.1  \
  -profile=dcomptb                                      \
  cmdr-csr.json | ./cfssljson -bare cmdr
fi

if [[ ! -f manage.pem ]]; then
echo "generating manage certs"
./cfssl gencert                                         \
  -ca=ca.pem                                            \
  -ca-key=ca-key.pem                                    \
  -config=ca-config.json                                \
  -hostname=foundry,server,127.0.0.1,localhost          \
  -profile=dcomptb                                      \
  manage-csr.json | ./cfssljson -bare manage
fi

if [[ ! -f wgd.pem ]]; then
echo "generating wgd certs"
./cfssl gencert                                         \
  -ca=ca.pem                                            \
  -ca-key=ca-key.pem                                    \
  -config=ca-config.json                                \
  -hostname=wgd,wgd.mergetb.test,127.0.0.1,localhost    \
  -profile=dcomptb                                      \
  wgd-csr.json | ./cfssljson -bare wgd
fi
