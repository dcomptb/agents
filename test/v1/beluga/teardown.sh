#!/bin/bash

if [[ $UID -ne 0 ]]; then
  echo "must be root to run"
  exit 1
fi

./stop-beluga.sh
rvn destroy
