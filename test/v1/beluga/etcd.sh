#!/bin/bash

addr=127.0.0.1

mkdir -p data

etcd \
  --name localhost \
  --data-dir `pwd`/data \
  --listen-client-urls http://$addr:2374 \
  --advertise-client-urls http://$addr:2374 \
  --listen-peer-urls http://$addr:2380 \
  --initial-advertise-peer-urls http://$addr:2380 \
  --initial-cluster localhost=http://$addr:2380 \
  --initial-cluster-state new

