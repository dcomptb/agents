---

- hosts: stor
  become: yes

  tasks:

  # install docker
  - name: "install packages necessary"
    apt:
      name: 
        - jq
      state: present

  - name: install docker (use script)
    script: get-docker.sh

  - name: install canopy
    include_role:
      name: canopy
    vars:
      server: yes
      client: yes
      arch: amd64
      platform: debian

  - name: do all the canopy things
    shell: "{{ item }}"
    with_items:
      - /usr/local/bin/canopy set bridge present sledbr stor
      - /usr/local/bin/canopy set vtep present 2 10.99.0.10 vtep2 stor -p eth1
      - /usr/local/bin/canopy set port master sledbr vtep2 stor
      - /usr/local/bin/canopy set port mtu 9216 eth1 stor
      - /usr/local/bin/canopy set port mtu 9166 vtep2 stor
      - /usr/local/bin/canopy set port link up sledbr,vtep2 stor
      - /usr/local/bin/canopy commit

  # okay now we can create docker container and attach to our bridge
  # first pull container
  - name: "get sledd container"
    shell: "docker pull mergetb/sledd:latest"

  - name: "get sledd nginx container"
    shell: "docker pull mergetb/slednginx:latest"

  - name: "kill all dockers"
    shell: "docker kill $(docker ps -q)"
    ignore_errors: yes

  - name: "clean sled image directory"
    shell: "rm -rf /var/img/*"
    ignore_errors: yes

  - name: "create host mount for images"
    shell: "mkdir -p /var/img/pxe"

  - name: "prefill /var/img"
    get_url: 
      url: "{{item.src}}"
      dest: "/var/img/{{item.dest}}"
    loop:
      - src: https://gitlab.com/mergetb/tech/images/-/jobs/artifacts/master/raw/debian/buster/debian-buster-raven.img?job=buster-raven
        dest: debian-buster-disk

      - src: https://gitlab.com/mergetb/tech/images/-/jobs/artifacts/master/raw/kernels/4.19/out/vmlinuz-4.19.0?job=k4.19
        dest: debian-buster-kernel

      - src: https://gitlab.com/mergetb/tech/images/-/jobs/artifacts/master/raw/kernels/4.19/out/initrd.img-4.19.0?job=k4.19
        dest: debian-buster-initramfs

      - src: https://gitlab.com/mergetb/tech/sled/-/jobs/artifacts/master/raw/build/generic-dhcp-initramfs.cpio?job=make
        dest: pxe/sledc-dhcp-initramfs

      - src: https://gitlab.com/mergetb/tech/sled/-/jobs/artifacts/master/raw/build/generic-bzImage?job=make
        dest: pxe/sledc-kernel

  - name: "start container on bridge"
    shell: "docker run -e SLEDB_HOST=172.30.0.2 -d --network host -v /var/img:/var/img mergetb/sledd"

  - name: "start nginx container on bridge"
    shell: "docker run -d --network host -v /var/img:/var/img mergetb/slednginx"

  # this is now in bgp land, configure the speaker
  - name: "remove bgp ip"
    shell: "ip addr del 10.99.0.10/32 dev lo"
    ignore_errors: yes

  - name: "set up the interface connected to ispine"
    shell: "ip link set eth1 up"

  - name: "delete ip"
    shell: "ip addr del 10.99.0.10/32 dev lo"
    ignore_errors: yes

  - name: "set up the local ip"
    shell: "ip addr add 10.99.0.10/32 dev lo"

  - name: "tear out bgp"
    shell: "gobgp global rib del 10.99.0.10/32 origin igp"
    ignore_errors: yes

  - name: setup GoBGP/Gobble
    include_role:
      name: gobble
    vars:
      interface: eth1
      with_gobgp: yes
      router_id: 10.99.0.10
      router_as: 64710
      peer_as: 64701

  - name: wait for
    shell: "/tmp/cogs/test/v1/bgp/wait-for-bgp.sh"

  - name: "set up bgp"
    shell: "gobgp global rib add 10.99.0.10/32 origin igp"

  - name: "delete sledd bridge an ip"
    shell: "ip addr del 172.30.0.3/16 dev sledbr"
    ignore_errors: yes

  - name: "give sledd bridge an ip"
    shell: "ip addr add 172.30.0.3/16 dev sledbr"

  - name: "del up bgp"
    shell: /tmp/cogs/test/v1/bgp/del-evpn.sh $(ip -br -j -4 -f link addr show sledbr | jq '.[4].address' | sed -e 's/"//g') 2 2 64710 10.99.0.10

  - name: "set up bgp"
    shell: /tmp/cogs/test/v1/bgp/add-evpn.sh $(ip -br -j -4 -f link addr show sledbr | jq '.[4].address' | sed -e 's/"//g') 2 2 64710 10.99.0.10
