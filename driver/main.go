package main

import (
	"context"
	"flag"
	"net"
	"time"

	"gitlab.com/dcomptb/cogs/pkg"
	"gitlab.com/mergetb/site/api"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

var (
	listen = flag.String("listen", "0.0.0.0:10000", "listen address")
)

type drv struct{}

// general driver interface methods ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (d *drv) Health(
	ctx context.Context, rq *site.HealthRequest) (*site.HealthResponse, error) {

	log.Info("health check")

	return &site.HealthResponse{}, nil

}

func (d *drv) NotifyIncoming(
	ctx context.Context, rq *site.IncomingRequest) (*site.IncomingResponse, error) {

	return &site.IncomingResponse{}, cogs.HandleIncoming(rq)

}

func (d *drv) Status(
	ctx context.Context, rq *site.StatusRequest) (*site.StatusResponse, error) {

	return cogs.HandleStatus(rq)

}

// node driver interface implementation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (d *drv) Setup(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, cogs.HandleNodeSetup(rq)

}

//TODO
func (d *drv) Recycle(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, cogs.HandleNodeRecycle(rq)

}

//TODO
func (d *drv) Configure(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

//TODO
func (d *drv) Restart(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

//TODO
func (d *drv) Reset(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

//TODO
func (d *drv) TurnOn(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

//TODO
func (d *drv) TurnOff(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

// link driver interface implementation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (d *drv) CreateLink(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, cogs.HandleLinkCreate(rq)

}

//TODO
func (d *drv) DestroyLink(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, cogs.HandleLinkDestroy(rq)

}

//TODO
func (d *drv) ModifyLink(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

//TODO
func (d *drv) ConnectLink(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

//TODO
func (d *drv) DisconnectLink(
	ctx context.Context, rq *site.MzRequest) (*site.MzResponse, error) {

	return &site.MzResponse{}, nil

}

func main() {

	log.Infof("dcomp merge driver %s", cogs.Version)

	flag.Parse()

	gsrv := grpc.NewServer()
	site.RegisterDriverServer(gsrv, &drv{})
	l, err := net.Listen("tcp", *listen)
	if err != nil {
		log.WithError(err).Fatal("failed to listen")
	}

	log.Infof("listening on %s", *listen)

	go func() {
		time.Sleep(3 * time.Second)
		cogs.RegisterWithCommander()
	}()

	gsrv.Serve(l)

}
