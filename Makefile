tests = build/harbor-test
svcs = build/rex build/driver
utils = build/cog

VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X gitlab.com/dcomptb/cogs/pkg.Version=$(VERSION)"

all: $(tests) $(svcs) $(utils)

commonlib_src=common/logging.go common/config.go common/etcd.go
fabriclib_src=fabric/linkplan.go fabric/canopy.go fabric/xir.go
coglib_src=pkg/cogs.go pkg/xir.go pkg/task.go pkg/store.go pkg/coglet.go pkg/cog-actions.go pkg/merge.go pkg/countset.go pkg/infranet.go pkg/node.go pkg/xpnet.go pkg/runtime.go pkg/mzdb.go pkg/containers.go pkg/bgp.go pkg/init.go pkg/gobgp-internal.go rex/plumbing.go pkg/plumbing.go pkg/etcd.go pkg/foundry.go pkg/util.go pkg/sled.go pkg/status.go $(fabriclib_src)

build/driver: driver/main.go $(commonlib_src) $(coglib_src) | build
	$(go-build)

build/rex: rex/main.go rex/*.go $(coglib_src) $(commonlib_src)| build
	$(go-build)

build/harbor-test: test/v1/tests/harbor_test.go $(coglib_src)
	$(go-build-test)

build/cog: util/cogctl/*.go $(coglib_src) | build
	$(go-build)

build/xmat: util/xmat/*.go $(coglib_src) | build
	$(go-build)

build:
	$(QUIET) mkdir -p build

.PHONY: clean
clean:
	$(QUIET) rm -rf build

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

QUIET=@
ifeq ($(V),1)
	QUIET=
endif

define build-slug
	@echo "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define go-build
	$(call build-slug,go)
	$(QUIET) go build -ldflags=$(LDFLAGS) -o $@ $(dir $<)*.go
endef

define go-build-file
	$(call build-slug,go)
	$(QUIET) go build -ldflags=$(LDFLAGS) -o $@ $<
endef

define go-build-test
	$(call build-slug,go-test)
	$(QUIET) go test -o $@ -c $<
endef

define protoc-build
	$(call build-slug,protoc)
	$(QUIET) protoc \
		-I . \
		-I ./vendor \
		-I $(GOPATH)/src \
		-I ./$(dir $@) \
		./$< \
		--go_out=plugins=grpc:.
endef
