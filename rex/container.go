package main

import (
	"fmt"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/dcomptb/cogs/pkg"
	"gitlab.com/mergetb/tech/rtnl"
)

func doContainerAction(action *cogs.Action) error {

	switch action.Action {

	case cogs.LaunchCtr:
		return doLaunchContainer(action)

	case cogs.CreateCtr:
		return doCreateContainer(action)

	case cogs.CreateCtrRecord:
		return doCreateContainerRecord(action)

	case cogs.PullCtrImage:
		return doPullContainerImage(action)

	case cogs.CreateCtrTask:
		return doCreateContainerTask(action)

	case cogs.ConfigCtrLo:
		return doConfigLo(action)

	case cogs.DeleteCtr:
		return doDeleteContainer(action)

	case cogs.DeleteCtrTask:
		return doDeleteContainerTask(action)

	case cogs.DeleteCtrRecord:
		return doDeleteContainerRecord(action)

	case cogs.DeleteNS:
		return doDeleteNS(action)

	default:
		return fmt.Errorf("unknown container action '%s'", action.Action)

	}

}

func doLaunchContainer(a *cogs.Action) error {

	spec := &cogs.ContainerSpec{}
	err := mapstructure.Decode(a.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid launch container data")
	}

	err = cogs.CreateContainerRecord(spec)
	if err != nil {
		return err
	}

	err = cogs.PullContainerImage(spec.Namespace, spec.Image)
	if err != nil {
		return err
	}

	err = cogs.SetupContainerLo(a.Mzid)
	if err != nil {
		return err
	}

	err = cogs.CreateContainer(spec)
	if err != nil {
		return err
	}

	err = cogs.CreateContainerTask(spec)
	if err != nil {
		return err
	}

	a.Complete = true

	cogs.Write(a.Task())
	return nil
}

func doCreateContainer(action *cogs.Action) error {

	spec := &cogs.ContainerSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid create container data")
	}

	err = cogs.CreateContainer(spec)
	if err != nil {
		return err
	}

	action.Complete = true
	return nil

}

func doCreateContainerRecord(action *cogs.Action) error {

	spec := &cogs.ContainerSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid create container data")
	}

	err = cogs.CreateContainerRecord(spec)
	if err != nil {
		return err
	}

	action.Complete = true

	return nil

}

func doCreateContainerTask(action *cogs.Action) error {

	spec := &cogs.ContainerSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid create container data")
	}

	err = cogs.CreateContainerTask(spec)
	if err != nil {
		return err
	}

	action.Complete = true
	return nil

}

func doPullContainerImage(action *cogs.Action) error {

	spec := &cogs.ContainerImageData{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid image name")
	}

	err = cogs.PullContainerImage(spec.Namespace, spec.Image)
	if err != nil {
		return err
	}

	action.Complete = true
	return nil

}

func doConfigLo(action *cogs.Action) error {

	err := cogs.SetupContainerLo(action.Mzid)
	if err != nil {
		return err
	}

	action.Complete = true
	return nil

}
func doDeleteContainer(action *cogs.Action) error {

	spec := &cogs.DeleteContainerData{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid contianer data")
	}

	err = cogs.DeleteContainer(spec.Name, spec.Namespace)
	if err != nil {
		return err
	}

	action.Complete = true
	return nil

}

func doDeleteContainerTask(action *cogs.Action) error {

	spec := &cogs.DeleteContainerData{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid container data")
	}

	err = cogs.DeleteContainerTask(spec.Name, spec.Namespace)
	if err != nil {
		return err
	}

	action.Complete = true
	return nil

}

func doDeleteContainerRecord(action *cogs.Action) error {

	spec := &cogs.DeleteContainerData{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid container data")
	}

	err = cogs.DeleteContainerRecord(spec.Name, action.Mzid, action)
	if err != nil {
		return err
	}

	action.Complete = true
	return nil

}

func doWithdrawMulticast(action *cogs.Action) error {

	spec := &cogs.EvpnWithdrawData{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid remove-vtep data '%#v %T'", action.Data, action.Data)
	}

	err = cogs.EvpnWithdrawMulticast(spec.Vni, spec.Vindex)
	if err != nil {
		return fmt.Errorf("failed withdraw multicast evpn route")
	}

	action.Complete = true
	return nil

}

func doDeleteNS(action *cogs.Action) error {

	netns := &cogs.NetNS{}
	err := mapstructure.Decode(action.Data, netns)
	if err != nil {
		return fmt.Errorf("invalid delete-ns data")
	}

	err = cogs.DestroyNetNS(netns)
	if err != nil {
		return fmt.Errorf("failed to delete netns")
	}

	action.Complete = true
	return nil

}

func doRemoveVtep(action *cogs.Action) error {

	vni, err := cogs.GetIntLike(action.Data)
	if err != nil {
		return fmt.Errorf("invalid remove-vtep data '%#v %T'", action.Data, action.Data)
	}

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer ctx.Close()

	lnk, err := rtnl.GetLink(ctx, fmt.Sprintf("vtep%d", vni))
	if err != nil {
		return err
	}

	err = lnk.Del(ctx)
	if err != nil {
		return err
	}

	action.Complete = true
	return nil

}
