package main

import (
	"fmt"
	"net"
	"os/exec"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/mergetb/tech/rtnl"
	"golang.org/x/sys/unix"

	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/dcomptb/cogs/pkg"
)

func doPlumbingAction(action *cogs.Action) error {

	switch action.Action {

	case cogs.CreateEnclave:
		return doCreateEnclave(action)

	case cogs.DestroyEnclave:
		return doDestroyEnclave(action)

	case cogs.AdvertiseMac:
		return doAdvertiseMac(action)

	default:
		return fmt.Errorf("unknown plumbing action '%s'", action.Action)
	}

}

// Enclave creation ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func doCreateEnclave(action *cogs.Action) error {

	spec := &cogs.EnclaveSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid enclave spec")
	}

	err = cogs.CreateInfrapodNS(action.Mzid)
	if err != nil {
		return err
	}

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return err
	}
	defer ctx.Close()

	err = enclaveBridge(spec, ctx)
	if err != nil {
		return err
	}

	err = enclaveIptables(action.Mzid, spec)
	if err != nil {
		return err
	}

	for _, ifx := range spec.Interfaces {

		ifx.Vindex = spec.Vindex

		err = cogs.CreateIfx(action.Mzid, ifx)
		if err != nil {
			return err
		}

	}

	err = nsDefaultRoute(action.Mzid, spec)
	if err != nil {
		return err
	}

	err = cogs.Write(spec)
	if err != nil {
		return common.ErrorEF("failed to save enclave spec", err, log.Fields{
			"mzid": action.Mzid,
		})
	}

	action.Complete = true
	return nil

}

// Enclave destruction ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func doDestroyEnclave(action *cogs.Action) error {

	fields := log.Fields{"mzid": action.Mzid}
	spec := &cogs.EnclaveSpec{Mzid: action.Mzid}
	err := cogs.Read(spec)
	if err != nil {
		return common.ErrorEF("failed to read enclave spec", err, fields)
	}

	delEnclaveIptables(action.Mzid, spec)

	err = cogs.DeleteInfrapodNS(action.Mzid)
	if err != nil {
		log.WithError(err).Warn("failed to delete infrapod netns")
	}

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		return common.ErrorEF("failed to open netlink context", err, fields)
	}
	defer ctx.Close()

	delEnclaveBridge(spec, ctx)

	for _, ifx := range spec.Interfaces {

		ifx.Vindex = spec.Vindex

		err = cogs.DeleteIfx(action.Mzid, ifx)
		if err != nil {
			return err
		}

	}

	delEnclaveVtep(spec, ctx)

	err = cogs.Delete(spec)
	if err != nil {
		log.WithFields(fields).Warning("failed to delete enclave spec")
	}

	action.Complete = true
	return nil

}

func doAdvertiseMac(action *cogs.Action) error {

	spec := &cogs.MacAdvSpec{}
	err := mapstructure.Decode(action.Data, spec)
	if err != nil {
		return fmt.Errorf("invalid macadv spec")
	}

	var ctx *rtnl.Context
	if spec.Netns == "" {

		ctx, err = rtnl.OpenDefaultContext()
		if err != nil {
			return common.ErrorE("rtnl default context", err)
		}

	} else {

		ctx, err = rtnl.OpenContext(spec.Netns)
		if err != nil {
			return common.ErrorE("rtnl context", err)
		}

	}
	defer ctx.Close()

	lnk, err := rtnl.GetLink(ctx, spec.Interface)
	if err != nil {
		return common.ErrorE("failed to get link", err)
	}

	err = cogs.EvpnAdvertiseMac(lnk, spec.Vni, spec.Vindex)
	if err != nil {
		return err
	}

	err = cogs.EvpnAdvertiseMulticast(spec.Vni, spec.Vindex)
	if err != nil {
		return err
	}

	action.Complete = true
	return nil

}

func enclaveBridge(spec *cogs.EnclaveSpec, ctx *rtnl.Context) error {

	br := rtnl.NewLink()
	br.Info.Name = fmt.Sprintf("mzbr%d", spec.Vindex)
	br.Info.Bridge = &rtnl.Bridge{VlanAware: false}

	err := br.Present(ctx)
	if err != nil {
		return common.ErrorEF("failed to ensure enclave mzbr", err, log.Fields{
			"name": br.Info.Name,
		})
	}

	err = br.Up(ctx)
	if err != nil {
		return common.ErrorEF("failed to bring upenclave mzbr", err, log.Fields{
			"name": br.Info.Name,
		})
	}

	return nil

}

func delEnclaveBridge(spec *cogs.EnclaveSpec, ctx *rtnl.Context) error {

	br := rtnl.NewLink()
	br.Info.Name = fmt.Sprintf("mzbr%d", spec.Vindex)
	br.Info.Bridge = &rtnl.Bridge{VlanAware: false}

	err := br.Absent(ctx)
	if err != nil {
		return common.ErrorEF("failed to remove enclave mzbr", err, log.Fields{
			"name": br.Info.Name,
		})
	}

	return nil

}

func delEnclaveVtep(spec *cogs.EnclaveSpec, ctx *rtnl.Context) error {

	vt := rtnl.NewLink()
	vt.Info.Name = fmt.Sprintf("vtep%d", spec.Vindex)

	err := vt.Absent(ctx)
	if err != nil {
		return common.ErrorEF("failed to remove enclave vtep", err, log.Fields{
			"name": vt.Info.Name,
		})
	}

	return nil

}

func nsDefaultRoute(mzid string, spec *cogs.EnclaveSpec) error {

	fields := log.Fields{
		"mzid": mzid,
	}

	ctx, err := rtnl.OpenContext(mzid)
	if err != nil {
		return common.ErrorEF("error opening mzn namespace", err, fields)
	}

	// from infrapod to host external subnet

	_, extnet, err := net.ParseCIDR(spec.ExternalSubnet)
	if err != nil {
		fields["subnet"] = spec.ExternalSubnet
		return common.ErrorEF("error parsing external subnet", err, fields)
	}
	dstlen, _ := extnet.Mask.Size()

	ifx, err := rtnl.GetLink(ctx, cogs.InfrapodMznIfx)
	if err != nil {
		fields["ifx"] = cogs.InfrapodMznIfx
		return common.ErrorEF("invalid infrapod mzn interface", err, fields)
	}

	pubnet := &rtnl.Route{
		Hdr:  unix.RtMsg{Dst_len: uint8(dstlen)},
		Dest: extnet.IP,
		Oif:  uint32(ifx.Msg.Index),
	}
	err = pubnet.Present(ctx)
	if err != nil {
		return common.ErrorEF("error creating infrapod host route", err, fields)
	}

	// default

	gwaddr := net.ParseIP(cogs.DefaultServiceGateway)
	if gwaddr == nil {
		fields["gateway"] = cogs.DefaultServiceGateway
		return common.ErrorEF("invalid external gateway", err, fields)
	}
	defroute := &rtnl.Route{
		Hdr:     unix.RtMsg{Dst_len: 0},
		Dest:    net.ParseIP("0.0.0.0"),
		Gateway: gwaddr,
	}

	err = defroute.Present(ctx)
	if err != nil {
		return common.ErrorEF("error creating infrapod default route", err, fields)
	}

	return nil

}

func enclaveIptables(mzid string, spec *cogs.EnclaveSpec) error {

	//src := fmt.Sprintf("172.31.0.%d", spec.Vindex)
	src := cogs.SvcAddress(spec.Vindex)

	cmd := exec.Command(
		"ip", "netns", "exec", mzid,
		"iptables", "-t", "nat", "-A",
		"POSTROUTING",
		"-s", "172.30.0.0/16",
		"!", "-d", "172.30.0.0/16",
		"-j", "SNAT", "--to-source", src,
	)

	out, err := cmd.CombinedOutput()
	if err != nil {
		return common.ErrorEF("enclave iptables failed", err, log.Fields{
			"args":   cmd.Args,
			"output": string(out),
		})
	}

	return nil

}

func delEnclaveIptables(mzid string, spec *cogs.EnclaveSpec) error {

	//src := fmt.Sprintf("172.31.0.%d", spec.Vindex)
	src := cogs.SvcAddress(spec.Vindex)

	cmd := exec.Command(
		"ip", "netns", "exec", mzid,
		"iptables", "-t", "nat", "-D",
		"POSTROUTING",
		"-s", "172.30.0.0/16",
		"!", "-d", "172.30.0.0/16",
		"-j", "SNAT", "--to-source", src,
	)

	out, err := cmd.CombinedOutput()
	if err != nil {
		return common.ErrorEF("enclave iptables failed", err, log.Fields{
			"args":   cmd.Args,
			"output": string(out),
		})
	}

	return nil

}
