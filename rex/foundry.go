package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"time"

	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/foundry/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/dcomptb/cogs/pkg"
)

const (
	foundryCert     = "/etc/foundry/manage.pem"
	foundryMgmtPort = 27000
)

func addFoundryConfig(spec *cogs.NodeSpec) error {

	return withFoundry(spec.SvcEndpoint,
		func(ctx context.Context, cli api.ManageClient) error {

			_, err := cli.Set(ctx, &api.SetRequest{
				Configs: []*api.MachineConfig{
					spec.Config,
				},
			})
			return err

		},
	)

}

func connect(endpoint string) (*grpc.ClientConn, api.ManageClient) {

	endpoint = fmt.Sprintf("%s:%d", endpoint, foundryMgmtPort)

	conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
	if err != nil {
		log.Fatal("foundry connection failed: %v", err)
	}
	client := api.NewManageClient(conn)

	return conn, client

}

func withFoundry(
	endpoint string, f func(context.Context, api.ManageClient) error) error {

	conn, cli := connect(endpoint)
	defer conn.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	return f(ctx, cli)

}

func tlsConfig(endpoint string) (credentials.TransportCredentials, error) {

	b, err := ioutil.ReadFile(foundryCert)
	if err != nil {
		return nil, err
	}
	cp := x509.NewCertPool()
	if !cp.AppendCertsFromPEM(b) {
		return nil, fmt.Errorf("credentials: failed to append certificates")
	}
	return credentials.NewTLS(&tls.Config{
		ServerName:         endpoint,
		RootCAs:            cp,
		InsecureSkipVerify: true,
	}), nil

}
