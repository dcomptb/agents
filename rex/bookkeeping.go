package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/dcomptb/cogs/pkg"
)

func doBookkeepingAction(action *cogs.Action) error {

	log.WithFields(log.Fields{"action": action.Action}).Info("begin bookkeeping action")

	switch action.Action {

	case cogs.DeleteMzinfo:

		mzi := &cogs.MzInfo{Mzid: action.Mzid}
		err := cogs.Delete(mzi)
		if err != nil {
			common.ErrorE("failed to delete mzinfo", err)
		}

		if err == nil {
			action.Complete = true
			return nil
		} else {
			log.WithError(err).Error("bookkeeping task failed")
			return err
		}

	}

	return nil
}
