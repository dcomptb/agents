package main

import (
	"context"
	"fmt"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	. "gitlab.com/dcomptb/cogs/common"
	"gitlab.com/mergetb/tech/beluga/pkg"
)

func withBeluga(f func(beluga.BelugaClient) error) error {

	cfg := GetConfig()

	conn, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.Beluga.Address, cfg.Beluga.Port),
		grpc.WithInsecure(),
	)
	if err != nil {
		log.WithError(err).Error("failed to dial beluga")
		return err
	}

	defer conn.Close()

	return f(beluga.NewBelugaClient(conn))

}

func belugaOn(device string, soft bool) error {

	return belugaFunc(beluga.DoRequest_On, device, soft)

}

func belugaOff(device string, soft bool) error {

	return belugaFunc(beluga.DoRequest_Off, device, soft)

}

func belugaCycle(device string, soft bool) error {

	return belugaFunc(beluga.DoRequest_Cycle, device, soft)

}

func belugaFunc(op beluga.DoRequest_Action, device string, soft bool) error {

	return withBeluga(func(c beluga.BelugaClient) error {

		_, err := c.Do(context.TODO(), &beluga.DoRequest{
			Action: op,
			Device: device,
			Soft:   soft,
		})

		return err

	})

}
