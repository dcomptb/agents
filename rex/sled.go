package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"time"

	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/dcomptb/cogs/common"
	cogs "gitlab.com/dcomptb/cogs/pkg"
	sled "gitlab.com/mergetb/tech/sled/pkg"
)

func parseAction(action *cogs.Action) (*cogs.ImageSpec, error) {
	// mac, sled.Image struct
	bindings := &cogs.ImageSpec{}
	// Parse the data struct in the data field
	err := mapstructure.Decode(action.Data, bindings)
	if err != nil {
		log.Infof("[ParseAction:Data] %#v", action.Data)
		log.Infof("[ParseAction] %#v", bindings)
		log.
			WithError(err).
			WithFields(log.Fields{"kind": "sled"}).
			Error("failed to parse sled action data")
		return nil, err
	}
	return bindings, nil
}

func sledImage(spec *cogs.NodeSpec, noverify bool) error {

	fields := log.Fields{"node": spec.Resource}

	cs := &sled.CommandSet{
		Write: &sled.WriteResponse{
			Device:    spec.Binding.Image.Blockdevice,
			Image:     spec.Binding.Image.Disk,
			Kernel:    spec.Binding.Image.Kernel,
			Initramfs: spec.Binding.Image.Initramfs,
		},
		Kexec: &sled.KexecResponse{
			Append:    spec.Binding.Image.Append,
			Kernel:    "/tmp/kernel",
			Initramfs: "/tmp/initrd",
		},
	}

	_, err := addCommandSet(cogs.HarborSvcAddr, spec.Mac, cs)
	if err != nil {
		return common.ErrorEF("add command set failed", err, fields)
	}

	if !noverify {
		ch := make(chan error)
		go waitForMac(cogs.HarborSvcAddr, spec.Mac, ch)
		err := <-ch
		if err != nil {
			return common.ErrorEF("wait for completion failed", err, fields)
		}
	}

	return nil
}

// FIXME: Add certificates for authenticated communitcation client <---> server
func initClient(addr string) (*grpc.ClientConn, sled.SledbClient, error) {
	conn, err := grpc.Dial(fmt.Sprintf("%s:9913", addr), grpc.WithInsecure())
	if err != nil {
		log.Errorf("could not connect to sled server - %v", err)
		return nil, nil, err
	}

	return conn, sled.NewSledbClient(conn), nil
}

func addCommandSet(addr, mac string, cmd *sled.CommandSet) (*sled.PutCommandResponse, error) {
	msTimeout := 500 * time.Millisecond
	conn, cli, err := initClient(addr)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	var resp *sled.PutCommandResponse

	// TODO: add more intelligent retry mechanism
	for i := 0; i < 10; i++ {
		ctx, cancel := context.WithTimeout(context.Background(), msTimeout)
		defer cancel()

		resp, err = cli.PutCommand(ctx, &sled.PutCommandRequest{Mac: mac, Command: cmd})
		if err == nil {
			break
		}
		log.
			WithFields(log.Fields{"kind": "sled"}).
			Warnf("sledCog->sledb[%d,500ms]: %v", i, err)

		time.Sleep(1 * time.Second)
	}

	if err != nil {
		return nil, err
	}
	return resp, nil
}

func getMacStatus(mac string, c sled.SledbClient) (*sled.GetCommandResponse, error) {

	fields := log.Fields{"mac": mac}

	ctx, cancel := context.WithTimeout(context.Background(), Timeout)
	defer cancel()
	cs, err := c.GetCommand(ctx, &sled.GetCommandRequest{Mac: mac})
	if err != nil {
		return nil, common.ErrorEF("sled status check failed", err, fields)
	}

	return cs, nil

}

func waitForMac(endpoint, mac string, ch chan error) {

	fields := log.Fields{"mac": mac}
	log.Debugf("waiting on mac: %s", mac)

	ch <- withSled(endpoint, func(c sled.SledbClient) error {

		for i := 0; i < 600; i++ {

			cs, err := getMacStatus(mac, c)
			if err != nil {
				return err
			}

			done, err := csComplete(cs.Command)
			if err != nil {
				return common.ErrorEF("sled failed", err, fields)
			}
			if done {
				log.WithFields(fields).Info("sled run success")
				return nil
			}
			time.Sleep(1 * time.Second)
		}

		return common.ErrorF("sled timeout", fields)

	})

}

func waitForSledCompletion(endpoint string, macs []string) error {

	ch := make(chan error)
	for _, mac := range macs {
		go waitForMac(endpoint, mac, ch)
	}

	for i := 0; i < len(macs); i++ {
		err := <-ch
		if err != nil {
			//TODO: reporting more granular diagnostics would be nice
			return fmt.Errorf("image failure")
		}
	}

	return nil

}

func csComplete(cs *sled.CommandSet) (bool, error) {

	if cs == nil {
		return false, fmt.Errorf("command set is nil")
	}

	complete := true

	if cs.Wipe != nil {
		done, err := taskComplete("wipe", cs.Wipe.Request)
		if err != nil {
			return false, err
		}
		complete = complete && done
	}

	if cs.Write != nil {
		done, err := taskComplete("write", cs.Write.Request)
		if err != nil {
			return false, err
		}
		complete = complete && done
	}

	if cs.Kexec != nil {
		done, err := taskComplete("kexec", cs.Kexec.Request)
		if err != nil {
			return false, err
		}
		complete = complete && done
	}

	return complete, nil

}

func taskComplete(name string, rq *sled.Request) (bool, error) {

	if rq.Status == sled.Request_error {
		return false, fmt.Errorf(name)
	}
	if rq.Status == sled.Request_ok {
		return true, nil
	}
	if rq.Status == sled.Request_ack && name == "kexec" {
		return true, nil
	}

	return false, nil
}

const (
	Timeout = 3 * time.Second
)

func withSled(endpoint string, f func(c sled.SledbClient) error) error {

	conn, cli, err := initClient(endpoint)
	if err != nil {
		return err
	}
	defer conn.Close()

	return f(cli)

}
