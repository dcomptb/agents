package main

import (
	"log"
	"os"
	"text/tabwriter"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"gitlab.com/dcomptb/cogs/pkg"
)

const (
	idAlphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_@"
)

var (
	blue  = color.New(color.FgBlue).SprintFunc()
	black = color.New(color.FgHiBlack).SprintFunc()
	white = color.New(color.FgWhite).SprintFunc()
	red   = color.New(color.FgRed).SprintFunc()
	green = color.New(color.FgGreen).SprintFunc()
)

var tw = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)

func main() {

	log.SetFlags(0)

	root := &cobra.Command{
		Use:   "cog",
		Short: "Interact with cogs",
	}

	version := &cobra.Command{
		Use:   "version",
		Short: "Print cogs library version",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			log.Print(cogs.Version)
		},
	}
	root.AddCommand(version)

	var autocomplete = &cobra.Command{
		Use:   "autocomplete",
		Short: "Generates bash completion scripts",
		Run: func(cmd *cobra.Command, args []string) {
			root.GenBashCompletion(os.Stdout)
		},
	}
	root.AddCommand(autocomplete)

	list := &cobra.Command{
		Use:   "list",
		Short: "List things",
	}
	root.AddCommand(list)

	show := &cobra.Command{
		Use:   "show",
		Short: "show things",
	}
	root.AddCommand(show)

	delete := &cobra.Command{
		Use:   "delete",
		Short: "delete things",
	}
	root.AddCommand(delete)

	create := &cobra.Command{
		Use:   "create",
		Short: "create things",
	}
	root.AddCommand(create)

	cogCmds(list)
	taskCmds(root, create, delete, list)
	nodeCmds(list)
	containerCmds(delete, list)
	mzCmds(root, show, delete, list)
	harborCmds(show, root)

	root.Execute()

}
