package main

import (
	"fmt"
	"log"
	"os/exec"
	"sort"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"github.com/teris-io/shortid"

	"gitlab.com/dcomptb/cogs/fabric"
	"gitlab.com/dcomptb/cogs/pkg"
	"gitlab.com/mergetb/site/api"
	"gitlab.com/mergetb/tech/canopy/lib"
	"gitlab.com/mergetb/xir/lang/go"
)

func mzCmds(root, show, delete, list *cobra.Command) {

	showMz := &cobra.Command{
		Use:   "mz <mzid>",
		Short: "show materialization info",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			mzInfo(args[0])
		},
	}
	show.AddCommand(showMz)

	listMzs := &cobra.Command{
		Use:   "mz [prefix]",
		Short: "list materializations",
		Run: func(cmd *cobra.Command, args []string) {
			pfx := ""
			if len(args) > 0 {
				pfx = args[0]
			}
			listMzs(pfx)
		},
	}
	list.AddCommand(listMzs)

	var all bool
	listMzTasks := &cobra.Command{
		Use:   "mztasks <mzid>",
		Short: "list materialization tasks",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			listMzTasks(args[0], all)
		},
	}
	listMzTasks.Flags().BoolVarP(&all, "all", "a", false, "list all tasks")
	list.AddCommand(listMzTasks)

	rematerialize := &cobra.Command{
		Use:   "remat <mzid>",
		Short: "rematerialize and experiment",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			rematerialize(args[0])
		},
	}
	root.AddCommand(rematerialize)

	tmux := &cobra.Command{
		Use:   "tmux <mzid>",
		Short: "tmux an experiment",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			tmux(args[0])
		},
	}
	root.AddCommand(tmux)

	var recordOnly bool
	del := &cobra.Command{
		Use:   "mz <mzid>",
		Short: "delete a materialization",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			delMz(args[0], recordOnly)
		},
	}
	del.Flags().BoolVarP(
		&recordOnly, "record-only", "r", false,
		"delete record only (do not dematerialize)",
	)
	delete.AddCommand(del)

}

func delMz(mzid string, recordOnly bool) {

	if recordOnly {
		mzi := &cogs.MzInfo{Mzid: mzid}
		err := cogs.Delete(mzi)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		demat(mzid)
	}

}

func demat(mzid string) {

	mzi := &cogs.MzInfo{Mzid: mzid}
	err := cogs.Read(mzi)
	if err != nil {
		log.Fatalf("could not read mzinfo %v", err)
	}

	// collect fragments ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// nodes
	var nodeFragments []*site.MzFragment
	for name, _ := range mzi.NodeInfo {

		nodeFragments = append(nodeFragments, &site.MzFragment{
			Resources: []string{name},
		})

	}
	// links
	var linkFragments []*site.MzFragment
	for _, spec := range mzi.LinkInfo {

		linkFragments = append(linkFragments, &site.MzFragment{
			Resources: spec.Links,
		})

	}

	// teardown ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	err = cogs.TeardownInfranet(mzid, mzi.Instance, nil, true)
	if err != nil {
		log.Printf("infranet teardown failed")
	}

	err = cogs.NodeRecycle(mzid, mzi.Instance, nodeFragments)
	if err != nil {
		log.Printf("node recycle failed")
	}

	err = cogs.DestroyLinks(mzid, mzi.Instance, linkFragments)
	if err != nil {
		log.Printf("link destroy failed")
	}

}

func listMzTasks(prefix string, all bool) {

	tasks, err := cogs.ListTasks()
	if err != nil {
		log.Fatal(err)
	}

	sort.Slice(tasks, func(i, j int) bool {

		t0, _ := time.Parse(cogs.TimeFormat, tasks[i].Timestamp)
		t1, _ := time.Parse(cogs.TimeFormat, tasks[j].Timestamp)

		return t0.Before(t1)

	})

	fmt.Fprintf(tw, "time\tid\tstage\tkind\tmzid\taction\tdeps\tcomplete\terror\n")
	for _, t := range tasks {
		for i, s := range t.Stages {
			for _, a := range s.Actions {

				if !strings.HasPrefix(a.Mzid, prefix) {
					continue
				}

				if a == nil {
					continue
				}

				if a.Complete && !all {
					continue
				}

				errmsg := ""
				if a.Error != nil {
					errmsg = *a.Error
				}

				fmt.Fprintf(tw, "%s\t%s\t%d\t%s\t%s\t%s\t%v\t%v\t%s\n",
					t.Timestamp, t.Id, i, a.Kind, a.Mzid, a.Action, t.Deps, a.Complete, errmsg)

			}
		}
	}
	tw.Flush()

}

func listMzs(prefix string) {

	mzs, err := cogs.ListMaterializations(prefix)
	if err != nil {
		log.Fatal(err)
	}

	for _, mz := range mzs {
		log.Println(mz)
	}

}

func mzInfo(mzid string) {

	topo, err := xir.FromFile("/etc/dcomp/tb-xir.json")
	if err != nil {
		log.Fatalf("failed to load testbed model: %v", err)
	}

	mzi := &cogs.MzInfo{Mzid: mzid}
	err = cogs.Read(mzi)
	if err != nil {
		log.Fatalf("could not read mzinfo %v", err)
	}

	// basics
	log.Println(blue("infranet"))
	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\n",
		black("vindex"),
		black("vni"),
		black("vid"),
		black("svcaddr"),
	)
	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\n",
		white(fmt.Sprintf("%d", mzi.Vindex)),
		white(fmt.Sprintf("%d", mzi.Vni)),
		white(fmt.Sprintf("%d", mzi.Vid)),
		white(cogs.SvcAddress(mzi.Vindex)),
	)
	tw.Flush()

	// containers

	containers, err := cogs.ListContainers(mzid)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(blue("containers"))
	for _, c := range containers {

		log.Print(white(c.ID()))

	}

	// node info

	log.Println(blue("nodes"))
	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
		black("node"),
		black("xpname"),
		black("image"),
		black("imac"),
		black("leaf"),
		black("access"),
		black("trunk"),
	)

	leafinfo := make(map[string]*LeafInfo)
	cc := canopy.NewClient()
	for node, _ := range mzi.NodeInfo {

		leaf, port, err := fabric.GetInfraLeaf(topo, node)
		if err != nil {
			log.Fatalf("failed to get infra leaf")
		}
		leafinfo[node] = &LeafInfo{Node: node, Leaf: leaf.Label(), Port: port.Label()}
		cc.GetPort(port.Label(), leaf.Label())

	}
	err = cc.Query()
	if err != nil {
		log.Fatalf("canopy error: %v", err)
	}

	for node, info := range mzi.NodeInfo {

		l := leafinfo[node]
		p := cc.Elements[l.Leaf][l.Port].GetPort()

		portstring := fmt.Sprintf("%s.%s", l.Leaf, l.Port)
		if p.State == canopy.UpDown_Up {
			portstring = green(portstring)
		} else {
			portstring = red(portstring)
		}

		fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
			white(node),
			white(info.XpName),
			white(info.Image.Image.Name),
			white(info.Nex.Mac),
			portstring,
			white(fmt.Sprintf("%d", p.Access)),
			white(fmt.Sprintf("%v", p.Tagged)),
		)

	}
	tw.Flush()

	// node config
	log.Println(blue("interfaces"))
	fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\n",
		black("node"),
		black("interface"),
		black("addrs"),
		black("mtu"),
		black("endpoint"),
	)

	for node, info := range mzi.NodeInfo {

		for _, ifx := range info.Config.Config.Interfaces {

			leaf, leafifx := leafport(topo, node, ifx.Name)

			fmt.Fprintf(tw, "%s\t%s\t%s\t%s\t%s\n",
				white(node),
				white(ifx.Name),
				white(fmt.Sprintf("%+v", ifx.Addrs)),
				white(fmt.Sprintf("%d", ifx.Mtu)),
				white(fmt.Sprintf("%s.%s", leaf, leafifx)),
			)

		}
	}
	tw.Flush()

	// links
	log.Println(blue("links"))
	for _, li := range mzi.LinkInfo {
		fmt.Fprintf(tw, "%s\t%s\n",
			black("vni"),
			black("vid"),
		)

		fmt.Fprintf(tw, "%s\t%s\n",
			white(fmt.Sprintf("%d", li.Vni)),
			white(fmt.Sprintf("%d", li.Vid)),
		)
		tw.Flush()

		fmt.Fprintf(tw, "%s\t%s\t%s\n",
			black("node"),
			black("interface"),
			black("multidegree"),
		)
		for _, e := range li.Endpoints {

			fmt.Fprintf(tw, "%s\t%s\t%s\n",
				white(e.Node),
				white(e.Interface),
				white(fmt.Sprintf("%v", e.Multidegree)),
			)
		}
		tw.Flush()

	}

}

func rematerialize(mzid string) {

	mzi := &cogs.MzInfo{Mzid: mzid}
	err := cogs.Read(mzi)
	if err != nil {
		log.Fatalf("could not read mzinfo %v", err)
	}

	// collect fragments ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	// nodes
	var nodeFragments []*site.MzFragment
	for name, _ := range mzi.NodeInfo {

		nodeFragments = append(nodeFragments, &site.MzFragment{
			Resources: []string{name},
		})

	}
	// links
	var linkFragments []*site.MzFragment
	for _, spec := range mzi.LinkInfo {

		linkFragments = append(linkFragments, &site.MzFragment{
			Resources: spec.Links,
		})

	}

	// teardown ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	err = cogs.TeardownInfranet(mzid, mzi.Instance, nil, false)
	if err != nil {
		log.Printf("infranet teardown failed")
	}

	err = cogs.NodeRecycle(mzid, mzi.Instance, nodeFragments)
	if err != nil {
		log.Printf("node recycle failed")
	}

	err = cogs.DestroyLinks(mzid, mzi.Instance, linkFragments)
	if err != nil {
		log.Printf("link destroy failed")
	}

	// stand up ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	err = cogs.InitInfranet(mzid, mzi.Instance, nil, false)
	if err != nil {
		log.Printf("infranet init failed")
	}

	// nodes
	err = cogs.NodeSetup(mzid, mzi.Instance, nodeFragments)
	if err != nil {
		log.Printf("node setup failed")
	}

	// links
	err = cogs.CreateLinks(mzid, mzi.Instance, linkFragments)
	if err != nil {
		log.Printf("link create failed")
	}

}

func tmux(mzid string) {

	mzi := &cogs.MzInfo{Mzid: mzid}
	err := cogs.Read(mzi)
	if err != nil {
		log.Fatalf("could not read mzinfo %v", err)
	}

	out, err := exec.Command("tmux", "start-server").CombinedOutput()
	if err != nil {
		log.Fatal(string(out))
	}

	session := strings.Replace(mzid, ".", "-", -1)
	out, err = exec.Command(
		"tmux", "new-session",
		"-x", "2000",
		"-y", "2000",
		"-d", "-s", session).CombinedOutput()
	if err != nil {
		log.Fatal(string(out))
	}

	for name, _ := range mzi.NodeInfo {

		out, err = exec.Command("tmux", "splitw").CombinedOutput()
		if err != nil {
			log.Fatal(string(out))
		}

		cmd := fmt.Sprintf("ssh %s", name)
		out, err = exec.Command("tmux", "send-keys", cmd, "C-m").CombinedOutput()
		if err != nil {
			log.Fatal(string(out))
		}

		out, err = exec.Command("tmux", "select-layout", "tiled").CombinedOutput()
		if err != nil {
			log.Fatal(string(out))
		}

	}

}

func tempMzid(prefix string) string {

	sid, err := shortid.New(1, idAlphabet, 4747)
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%s-%s", prefix, sid.MustGenerate())

}

func tempMzInstance() string {

	sid, err := shortid.New(1, idAlphabet, 4747)
	if err != nil {
		log.Fatal(err)
	}
	return sid.MustGenerate()

}

type LeafInfo struct {
	Node string
	Leaf string
	Port string
}

func leafport(topo *xir.Net, node, ifx string) (string, string) {

	_, _, xnode := topo.GetNodeByName(node)
	if xnode == nil {
		_, _, xnode = topo.GetNode(node)
		if xnode == nil {
			log.Fatalf("node %s not found in model", node)
		}
	}

	var ep *xir.Endpoint
	for _, e := range xnode.Endpoints {
		if e.Label() == ifx {
			ep = e
			break
		}
	}
	if ep == nil {
		log.Fatalf("interface %s.%s not found in model", node, ifx)
	}
	var nbr *xir.Endpoint
	for _, x := range ep.Neighbors {
		nbr = x.Endpoint
	}
	if nbr == nil {
		log.Fatalf("interface %s.eth0 not connected in model", node)
	}

	return nbr.Parent.Label(), nbr.Label()

}
