package main

import (
	"fmt"
	"log"
	"sort"

	"github.com/spf13/cobra"
	"gitlab.com/dcomptb/cogs/pkg"
)

func nodeCmds(list *cobra.Command) {

	listNodes := &cobra.Command{
		Use:   "nodes",
		Short: "list nodes",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			listNodes()
		},
	}
	list.AddCommand(listNodes)

}

func listNodes() {

	nodes, err := cogs.ListNodes()
	if err != nil {
		log.Fatal(err)
	}

	sort.Slice(nodes, func(i, j int) bool {
		return nodes[i].Name < nodes[j].Name
	})

	fmt.Fprintf(tw, "%s\t%s", "node", "mzid")
	for _, x := range nodes {

		fmt.Fprintf(tw, "%s\t%s\n", x.Name, x.Mzid)

	}

	tw.Flush()

}
