package main

import (
	"log"
	"testing"

	"gitlab.com/dcomptb/cogs/pkg"
	"gitlab.com/mergetb/xir/lang/go"
)

func TestLeafMapping(t *testing.T) {

	tb := DCompTB()
	net := tb.Net()
	net.ToFile("dcomp-xir.json")

	net, err := xir.FromFile("dcomp-xir.json")
	if err != nil {
		t.Fatal(err)
	}

	_, _, x := net.GetNode("m47")
	if x == nil {
		log.Fatal("m47 not found")
	}

	ep := x.GetEndpoint("eth0")
	if ep == nil {
		log.Fatal("m47::eth0 not found")
	}

	leafport := cogs.GetNodeLeaf(ep)
	if leafport == nil {
		log.Fatal("m47::eth0 leafport not found")
	}

	if leafport.Parent.Label() != "il0" {
		log.Fatalf("il0 != %s", leafport.Parent.Label())
	}

}
