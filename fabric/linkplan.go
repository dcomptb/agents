package fabric

import (
	"fmt"

	"gitlab.com/dcomptb/cogs/pkg"
	"gitlab.com/mergetb/xir/lang/go"
	"gitlab.com/mergetb/xir/lang/go/sys"
)

// leaf -> port -> AccessPort
type AccessMap map[*xir.Node]map[*xir.Endpoint]AccessPort

func (m AccessMap) Add(leaf *xir.Node, ep *xir.Endpoint, value AccessPort) {

	_, ok := m[leaf]
	if !ok {
		m[leaf] = make(map[*xir.Endpoint]AccessPort)
	}
	m[leaf][ep] = value

}

// fabric -> port -> TrunkPort
type TrunkMap map[*xir.Node]map[*xir.Endpoint]TrunkPort

func (m TrunkMap) Add(node *xir.Node, ep *xir.Endpoint, value TrunkPort) {

	_, ok := m[node]
	if !ok {
		m[node] = make(map[*xir.Endpoint]TrunkPort)
	}
	m[node][ep] = value

}

// fabirc -> interface -> Vtep
type VtepMap map[*xir.Node]map[string]Vtep

func (m VtepMap) Add(node *xir.Node, name string, value Vtep) {

	_, ok := m[node]
	if !ok {
		m[node] = make(map[string]Vtep)
	}
	m[node][name] = value

}

// LinkPlan defines a deployment plan for a link in terms of the ports and
// virtual interfaces that must be created to materialize it.
type LinkPlan struct {

	// materialization id
	Mzid string

	// id of the experiment link this plan is being created for
	XlinkId string

	LinkKind cogs.LinkKind

	AccessMap AccessMap
	TrunkMap  TrunkMap
	VtepMap   VtepMap

	ver int64
}

func (l *LinkPlan) Key() string {
	return fmt.Sprintf("/linkplan/%s/%s", l.Mzid, l.XlinkId)
}
func (l *LinkPlan) GetVersion() int64  { return l.ver }
func (l *LinkPlan) SetVersion(v int64) { l.ver = v }
func (l *LinkPlan) Value() interface{} { return l }

func (lp LinkPlan) String() string {

	var result string

	result += "access\n"
	for node, ports := range lp.AccessMap {
		for endpoint, port := range ports {
			result += fmt.Sprintf("%s:%s %+v\n", node.Label(), endpoint.Label(), port)
		}
	}

	result += "trunk\n"
	for node, ports := range lp.TrunkMap {
		for endpoint, port := range ports {
			result += fmt.Sprintf("%s:%s %+v\n", node.Label(), endpoint.Label(), port)
		}
	}

	result += "vtep\n"
	for node, ports := range lp.VtepMap {
		for name, port := range ports {
			result += fmt.Sprintf("%s:%s %+v\n", node.Label(), name, port)
		}
	}

	return result
}

type AccessPort struct {
	Name string
	Vid  int
}

type TrunkPort struct {
	Name string
	Vids []int
}

type Vtep struct {
	Name     string
	Vid      int
	Vni      int
	Bridge   string
	TunnelIp string
}

var PersistentTrunks = []int{
	2, // harbor never goes away
}

func NewLinkPlan(spec cogs.LinkSpec) (*LinkPlan, error) {

	topo, err := cogs.LoadXIR()
	if err != nil {
		return nil, err
	}

	accessMap, leafTrunkMap, err := MapLeaves(spec, topo)
	if err != nil {
		return nil, err
	}

	trunkMap, vtepMap, err := MapFabrics(spec, accessMap, topo)
	if err != nil {
		return nil, err
	}

	for k, v := range leafTrunkMap {
		trunkMap[k] = v
	}

	result := &LinkPlan{
		AccessMap: accessMap,
		TrunkMap:  trunkMap,
	}

	// determine whether or not to add vteps to the plan. in the case of
	// infrastructure, we always add vteps as there are services elsewhere in the
	// testbed such as nex and rally that always require a routed link across the
	// testbed switching mesh. in the case of an experiment link, we only create a
	// vtep if the link crosses more than one fabric, otherwise simple vlans at
	// the leaf layer are sufficient to plumb the connectivity.
	if spec.Kind == cogs.InfraLink || len(vtepMap) > 1 {
		result.VtepMap = vtepMap
	}

	return result, nil
}

func MapLeaves(spec cogs.LinkSpec, topo *xir.Net) (AccessMap, TrunkMap, error) {

	amap := make(AccessMap)
	tmap := make(TrunkMap)

	for _, e := range spec.Endpoints {

		leaf, leafport, err := GetLeaf(topo, e.Node, e.Interface)
		if err != nil {
			return nil, nil, err
		}

		if e.Multidegree {

			tmap.Add(leaf, leafport, TrunkPort{
				Name: leafport.Label(),
				Vids: []int{spec.Vid},
			})

		} else {

			amap.Add(leaf, leafport, AccessPort{
				Name: leafport.Label(),
				Vid:  spec.Vid,
			})

		}

	}

	return amap, tmap, nil

}

func MapFabrics(spec cogs.LinkSpec, amap AccessMap, topo *xir.Net) (
	TrunkMap, VtepMap, error) {

	tmap := make(TrunkMap)
	vmap := make(VtepMap)

	for leaf, _ := range amap {

		fabric, leafUplink, fabricDownlink, err := getFabric(leaf)
		if err != nil {
			return nil, nil, err
		}

		// uplink
		tmap.Add(leaf, leafUplink, TrunkPort{
			Name: linkName(leafUplink),
			Vids: []int{spec.Vid},
		})

		// downlink
		tmap.Add(fabric, fabricDownlink, TrunkPort{
			Name: linkName(fabricDownlink),
			Vids: []int{spec.Vid},
		})

		// vtep
		if spec.Vni == cogs.HarborInfraVNI {
			continue // harbor vteps are static
		}
		cfg := getVxlanConfig(fabric)
		if cfg == nil {
			return nil, nil, fmt.Errorf("no sysconfig found")
		}

		vname := fmt.Sprintf("vtep%d", spec.Vni)
		vmap.Add(fabric, vname, Vtep{
			Name:     vname,
			Vni:      spec.Vni,
			Vid:      spec.Vid,
			TunnelIp: cfg.TunnelIp,
			Bridge:   cfg.Bridge,
		})

	}

	return tmap, vmap, nil

}

// returns the name of the link, if the link is a part of a bond, returns the
// name of the bond
func linkName(x *xir.Endpoint) string {

	name := x.Label()
	bond := sys.GetBond(x)
	if bond != nil {
		name = bond.Name
	}

	return name

}
