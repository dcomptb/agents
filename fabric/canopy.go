package fabric

import (
	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/mergetb/tech/canopy/lib"
)

// access port ----------------------------------------------------------------

func (ap AccessPort) Create() *canopy.Property {
	return ap.modify(canopy.Presence_Present)
}

func (ap AccessPort) Destroy() *canopy.Property {
	return ap.modify(canopy.Presence_Absent)
}

func (ap AccessPort) modify(presence canopy.Presence) *canopy.Property {

	return &canopy.Property{
		Element: ap.Name,
		Value: &canopy.Property_Access{&canopy.Access{
			Presence: presence,
			Vid:      int32(ap.Vid),
		}},
	}

}

// trunk port -----------------------------------------------------------------

func (tp TrunkPort) Create() *canopy.Property {
	return tp.modify(canopy.Presence_Present)
}

func (tp TrunkPort) Destroy() *canopy.Property {
	return tp.modify(canopy.Presence_Absent)
}

func (tp TrunkPort) modify(presence canopy.Presence) *canopy.Property {

	var vids []int32
	for _, x := range tp.Vids {

		persistent := false
		for _, p := range PersistentTrunks {
			if x == p {
				persistent = true
			}
		}
		if persistent && presence == canopy.Presence_Absent {
			continue
		}

		vids = append(vids, int32(x))
	}

	return &canopy.Property{
		Element: tp.Name,
		Value: &canopy.Property_Tagged{&canopy.Tagged{
			Presence: presence,
			Vids:     vids,
		}},
	}

}

// vtep -----------------------------------------------------------------------

func (v Vtep) Create() []*canopy.Property {
	return v.modify(canopy.Presence_Present)
}

func (v Vtep) Destroy() []*canopy.Property {
	return v.modify(canopy.Presence_Absent)
}

func (v Vtep) modify(presence canopy.Presence) []*canopy.Property {

	cfg := common.GetConfig()

	switch presence {

	// several things to do to bring up a vtep
	case canopy.Presence_Present:
		return []*canopy.Property{
			// establish vtep
			{
				Element: v.Name,
				Value: &canopy.Property_Vtep{&canopy.VtepPresence{
					Presence: presence,
					Vni:      int32(v.Vni),
					TunnelIp: v.TunnelIp,
				}},
			},
			// add vtep to bridge
			{
				Element: v.Name,
				Value: &canopy.Property_Master{&canopy.Master{
					Value:    v.Bridge,
					Presence: presence,
				}},
			},

			// set vtep access vid
			{
				Element: v.Name,
				Value: &canopy.Property_Access{&canopy.Access{
					Vid:      int32(v.Vid),
					Presence: presence,
				}},
			},
			// set vtep mtu
			{
				Element: v.Name,
				Value:   &canopy.Property_Mtu{int32(cfg.Net.VtepMtu)},
			},
			// set the link state to up
			{
				Element: v.Name,
				Value:   &canopy.Property_Link{canopy.UpDown_Up},
			},
		}

	// only one thing to do to bring down a vtep
	case canopy.Presence_Absent:
		return []*canopy.Property{
			{
				Element: v.Name,
				Value: &canopy.Property_Vtep{&canopy.VtepPresence{
					Presence: presence,
					Vni:      int32(v.Vni),
					TunnelIp: v.TunnelIp,
				}},
			},
		}

	}

	return nil

}

// link plan ------------------------------------------------------------------

func (lp LinkPlan) Create() CanopyOps {

	return lp.modify(canopy.Presence_Present)

}

func (lp LinkPlan) Destroy() CanopyOps {

	return lp.modify(canopy.Presence_Absent)

}

func (lp LinkPlan) modify(presence canopy.Presence) CanopyOps {

	result := make(CanopyOps)

	for host, ports := range lp.AccessMap {
		for _, port := range ports {
			result[host.Label()] = append(result[host.Label()], port.modify(presence))
		}
	}

	for host, ports := range lp.TrunkMap {
		for _, port := range ports {
			result[host.Label()] = append(result[host.Label()], port.modify(presence))
		}
	}

	for host, ports := range lp.VtepMap {
		for _, port := range ports {
			result[host.Label()] = append(result[host.Label()], port.modify(presence)...)
		}
	}

	return result

}

// canopy ops -----------------------------------------------------------------

type CanopyOps map[string][]*canopy.Property

func (ops CanopyOps) Exec() error {

	cc := canopy.NewClient()

	for host, props := range ops {
		for _, prop := range props {
			cc.Add(prop, host)
		}
	}

	return cc.Commit()

}
