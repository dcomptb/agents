package fabric

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"gitlab.com/dcomptb/cogs/common"
	"gitlab.com/dcomptb/cogs/pkg"
	"gitlab.com/mergetb/xir/lang/go"
	"gitlab.com/mergetb/xir/lang/go/sys"
	"gitlab.com/mergetb/xir/lang/go/tb"
)

func GetInfraLeaf(topo *xir.Net, node string) (*xir.Node, *xir.Endpoint, error) {

	_, _, n := topo.GetNode(node)
	if n == nil {
		return nil, nil, fmt.Errorf("node '%s' not found", node)
	}

	for _, ep := range n.Endpoints {

		leafport := cogs.GetNodeLeaf(ep)
		if leafport == nil {
			continue
		}
		leaf := leafport.Parent

		if tb.HasRole(leaf, tb.Leaf) && tb.HasRole(leaf, tb.InfraSwitch) {
			return leaf, leafport, nil
		}

	}

	return nil, nil, fmt.Errorf("not found")

}

func GetLeaf(tb *xir.Net, node, ifx string) (*xir.Node, *xir.Endpoint, error) {

	_, _, n := tb.GetNode(node)
	if n == nil {
		return nil, nil, fmt.Errorf("node '%s' not found", node)
	}

	ep := n.GetEndpoint(ifx)
	if ep == nil {
		return nil, nil, fmt.Errorf("node interface '%s' not found", ifx)
	}

	leafport := cogs.GetNodeLeaf(ep)
	if leafport == nil {
		return nil, nil, fmt.Errorf("node '%s' has no leaf for ifx %s", node, ifx)
	}
	leaf := leafport.Parent

	return leaf, leafport, nil

}

func getFabric(leaf *xir.Node) (*xir.Node, *xir.Endpoint, *xir.Endpoint, error) {

	for _, e := range leaf.Endpoints {
		for _, n := range e.Neighbors {
			x := n.Endpoint.Parent
			if tb.HasRole(x, tb.Fabric) || tb.HasRole(x, tb.Spine) {
				return n.Endpoint.Parent, e, n.Endpoint, nil
			}
		}
	}

	return nil, nil, nil, fmt.Errorf("fabric not found")

}

type VxlanConfig struct {
	TunnelIp string
	Bridge   string
}

func getVxlanConfig(n *xir.Node) *VxlanConfig {

	fields := log.Fields{"switch": n.Label()}

	sysconf := sys.SysConfig(n)
	if sysconf == nil {
		common.ErrorF("sysconfig not found", fields)
		return nil
	}

	if sysconf.Evpn == nil {
		common.ErrorF("evpn config not found", fields)
		return nil
	}

	if sysconf.PrimaryBridge == nil {
		common.ErrorF("primary bridge config not found", fields)
		return nil
	}

	return &VxlanConfig{
		TunnelIp: sysconf.Evpn.TunnelIP,
		Bridge:   sysconf.PrimaryBridge.Name,
	}

}
